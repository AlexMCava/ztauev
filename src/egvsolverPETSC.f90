module egv_solver
#include <petsc/finclude/petscksp.h>
    use petscksp
    use precision_mod,   only: RP
    use paramlistClass
    use jac_matrix
    use math_tools
    implicit none

! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
!                   PETSc Variable declarations
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Vec              x,b,u
    Mat              A, A_noshift, Atemp
    Mat              M, Minv
    KSP              ksp
    PetscReal        norm,t1,t2
    PetscInt         nrows, nnzproc
    PetscInt         RowStart, RowEnd
    PetscErrorCode   ierr
    PetscMPIInt      rank, size
    PetscLogDouble   residual_memory, ALLOCATION_memory
#if defined(PETSC_HAVE_MUMPS)
    PC               pc
    Mat              F
    PetscInt         ival,icntl,infog34
    PetscReal        cntl,rinfo12,rinfo13,val
#endif

! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
! Object 'method' contains all the functions and subroutines for the EGV solving
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    type method
        integer, pointer                 :: row_ptr(:)
        integer, pointer                 :: col_ind(:)
        complex(kind=RP),pointer         :: data(:)
        integer                          :: dimPrb, nnz
        logical, private                 :: islu       ! logical value to indicate if the matrix has been factorized

        ! Arnoldi iteration arrays
        COMPLEX(kind=RP), allocatable    :: vr(:,:)
        COMPLEX(kind=RP), allocatable, private :: h(:,:)

        COMPLEX(kind=RP), allocatable    :: v(:,:)  ! Array of eigenvectors
        COMPLEX(kind=RP), allocatable    :: EIG(:)  ! Array of eigenvalues

    CONTAINS
        procedure          :: solve
        procedure, private :: mass_matrix
        procedure, private :: preconditioning
        procedure, private :: factor_matrix
        procedure, private :: krylov_solver
        procedure, private :: arnoldi_iteration
        procedure, private :: arp_arnoldi
        procedure, private :: save_eig
        procedure          :: load_hk
        procedure          :: save_hk
    end type

    interface method
        procedure new_problem
    end interface

CONTAINS
!! ======================================================================== !!
    function new_problem(S, params) result(new)
        !- Constructor for the EGV solver. Define main variables from the user
        !  parameters file, initialize arrays and do a quick error-check.

        type(method)                        :: new
        type(amatrix), intent(inout), target:: S
        type(paramlist),      intent(inout) :: params
        complex(8), allocatable             :: val_proc(:)
        integer, allocatable                :: row_proc(:), col_proc(:)
        character(len=250)                  :: pout
        integer                             :: i

        new%islu = .false.
        new%dimPrb = S%nvar
        new%nnz    = S%nnz

        !! Correction of index for PETSc reading (C style)
        do i = 1, new%dimPrb+1
            S%row_ptr(i) = S%row_ptr(i) - 1
        end do
        do i = 1, new%nnz
            S%col_ind(i) = S%col_ind(i) - 1
            ! S%IRN(i) = S%IRN(i) - 1
        end do

        call MPI_Comm_rank(PETSC_COMM_WORLD, rank, ierr);CHKERRA(ierr)
        CALL MPI_Comm_size(PETSC_COMM_WORLD, size, ierr);CHKERRA(ierr)
        CALL cpu_time ( t1 )

        CALL PetscMemorySetGetMaximumUsage(ierr)
        CALL PetscMemoryGetCurrentUsage(residual_memory, ierr)

        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        !! Generate PETSc Matrix
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        write(pout,'(A)') '    Creating PETSc MAT object...\n'
        call PetscPrintf(PETSC_COMM_WORLD, trim(pout),ierr)

        CALL MatCreate(PETSC_COMM_WORLD, A, ierr);CHKERRA(ierr)
        CALL MatSetSizes(A, PETSC_DECIDE, PETSC_DECIDE, &
                            new%dimPrb, new%dimPrb, ierr);CHKERRA(ierr)
        CALL MatSetUp(A, ierr);CHKERRA(ierr)
        !! Alows user input
        !! CALL MatSetFromOptions(A, ierr);CHKERRA(ierr)
        !! Returns the range of matrix rows owned by this processor
        CALL MatGetOwnershipRange(A, RowStart, RowEnd, ierr);CHKERRA(ierr)

        ! write(*,*) " PROCESSOR N·", rank+1, "OF", size
        write(*,'(A,I2,A,I2,A,I10,A,I10)') " PROCESSOR N·", rank+1, &
                                " OF", size," USES FROM", RowStart, " TO", RowEnd

        ! Simple algorithm to divide information between processors
        nrows = RowEnd - Rowstart
        allocate(row_proc(nrows+1))
        row_proc(1) = 0
        do i = 2, nrows+1
            row_proc(i) = S%row_ptr(RowStart+i) - S%row_ptr(RowStart+1)
        end do

        nnzproc = 0
        do i = 1, nrows
            nnzproc = nnzproc + row_proc(i+1) - row_proc(i)
        end do

        write(*,'(A,I2,A,I10,A)') " PROCESSOR N·", rank+1, " USES", nnzproc, ' NNZ ELEM'
        allocate(col_proc(nnzproc), val_proc(nnzproc))

        do i = 1, nnzproc
            col_proc(i) = S%col_ind ( S%row_ptr(RowStart + 1) + i )
            val_proc(i) = S%data    ( S%row_ptr(RowStart + 1) + i )
        end do

        ! write(*,*) " PROCESSOR N·", rank+1, "DATA", real(val_proc)
        call MPI_BARRIER(PETSC_COMM_WORLD, ierr);CHKERRA(ierr)
        call PetscPrintf(PETSC_COMM_WORLD, '\n',ierr);CHKERRA(ierr)

        ! PARALLEL VS SEQUENTIAL SELECTOR & PRE-ALLOCATION
        IF (RANK.EQ.0) write(*,*) '    PETSc CSR preallocation...'
        IF (SIZE.EQ.1) then
            CALL MatSetType(A, MATSEQAIJ, ierr);CHKERRA(ierr)
            CALL MatSeqAIJSetPreallocationCSR(A, row_proc, col_proc, &
            &                                   val_proc, ierr);CHKERRA(ierr)
        else IF (SIZE.GT.1) then
            CALL MatSetType(A, MATMPIAIJ, ierr);CHKERRA(ierr)
            CALL MatMPIAIJSetPreallocationCSR(A, row_proc, col_proc, &
            &                                   val_proc, ierr);CHKERRA(ierr)
        end if

        deallocate(row_proc, col_proc, val_proc)

        !! Finalize the definition of the matrix A in SLEPC
        write(pout,'(a)') '    Assembling Matrix...\n'
        call PetscPrintf(PETSC_COMM_WORLD, trim(pout),ierr);CHKERRA(ierr)
        CALL MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY, ierr);CHKERRA(ierr)
        CALL MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY, ierr);CHKERRA(ierr)
        call MPI_BARRIER(PETSC_COMM_WORLD, ierr);CHKERRA(ierr)
        CALL PetscMemoryGetCurrentUsage(ALLOCATION_memory, ierr);CHKERRA(ierr)
        ALLOCATION_memory = (ALLOCATION_memory - residual_memory)/1048576

        CALL cpu_time ( t2 )
        IF (RANK.EQ.0) then
            write(*,*)
            write(*,*) "time request for A Matrix allocation: ", t2-t1
            write(*,*) "MEMORY request for A Matrix allocation: ", &
            &               ALLOCATION_memory
            write(*,*) "Created A Matrix, ierr=", ierr
            write(*,*)
        end if

        !! Generation of mass matrix (volume matrix)
        IF(rank.eq.0) print*, 'Generating Mass Matrix...'
        CALL mass_matrix(new, S)

        !! Duplicate A matrix before aplying any preconditioning.
        !! This is necessary for later error and Eigenmode calculation.
        call MatDuplicate(A, MAT_COPY_VALUES, A_noshift, ierr);CHKERRA(ierr)

        !! Transforms A*x=lambda*M*x into Inv(M)*A*x=lambda*x
        IF (params%scalejmatrix) then
            call MatMatMult(Minv, A, MAT_INITIAL_MATRIX, PETSC_DEFAULT_REAL, Atemp, ierr);CHKERRA(ierr)
            call MatDestroy(A, ierr);CHKERRA(ierr)
            call MatDuplicate(Atemp, MAT_COPY_VALUES, A, ierr);CHKERRA(ierr)
            call MatDestroy(Atemp, ierr);CHKERRA(ierr)
            params%generalized = .false.
        END IF

        !! Undo of correction of index for PETSc reading (C style)
        !! Creation of pointers to CRS matrix information for Eigenmode calculation
        do i=1,new%dimPrb+1
            S%row_ptr(i) = S%row_ptr(i) + 1
        end do
        do i=1,new%nnz
            S%col_ind(i) = S%col_ind(i) + 1
        end do

        new%data => S%data
        new%row_ptr => S%row_ptr
        new%col_ind => S%col_ind

    end function new_problem
    !! ======================================================================== !!
        subroutine mass_matrix(this, S)
            class(method), intent(inout)  :: this
            type(amatrix), intent(in)     :: S

            integer, allocatable          :: irn_proc(:), col_proc(:)
            complex(kind=RP), allocatable :: val_proc(:), val_proc_inv(:)
            complex(kind=RP)              :: one
            real(kind=RP), allocatable    :: volumes(:)
            integer                       :: i, eq, npoints, neq, pt

            one  = (1.0d0, 0.0d0)
            neq = S%neq

            npoints = ubound(S%vol, dim=1)
            ! print*, 'npoints = ', npoints
            !! Replicate volumes for each point NVAR times
            allocate(volumes(this%dimPrb))
            do i = 1, npoints
                pt = i*neq-(neq-1)
                volumes(pt) = S%vol(i) ! read volumes
                do eq = 1, neq-1
                    volumes(pt+eq) = volumes(pt) ! insert extra values for volumes
                end do
            end do

            ! Create Matrices of size N for M and M_inv
            CALL MatCreate(PETSC_COMM_WORLD, M, ierr);CHKERRA(ierr)
            CALL MatSetSizes(M, PETSC_DECIDE, PETSC_DECIDE, &
                            this%dimPrb, this%dimPrb, ierr);CHKERRA(ierr)
            IF (SIZE.EQ.1) then
                CALL MatSetType(M, MATSEQAIJ, ierr);CHKERRA(ierr)
                CALL MatSeqAIJSetPreallocation(M, 1, PETSC_NULL_INTEGER, ierr);CHKERRA(ierr)
            else IF (SIZE.GT.1) then
                CALL MatSetType(M, MATMPIAIJ, ierr);CHKERRA(ierr)
                CALL MatMPIAIJSetPreallocation(M, 1, PETSC_NULL_INTEGER, &
                                    0, PETSC_NULL_INTEGER, ierr);CHKERRA(ierr)
            end if

            CALL MatCreate(PETSC_COMM_WORLD, Minv, ierr);CHKERRA(ierr)
            CALL MatSetSizes(Minv, PETSC_DECIDE, PETSC_DECIDE, &
                            this%dimPrb, this%dimPrb, ierr);CHKERRA(ierr)
            IF (SIZE.EQ.1) then
                CALL MatSetType(Minv, MATSEQAIJ, ierr);CHKERRA(ierr)
                CALL MatSeqAIJSetPreallocation(Minv, 1, PETSC_NULL_INTEGER, ierr);CHKERRA(ierr)
            else IF (SIZE.GT.1) then
                CALL MatSetType(Minv, MATMPIAIJ, ierr);CHKERRA(ierr)
                CALL MatMPIAIJSetPreallocation(Minv, 1, PETSC_NULL_INTEGER, &
                                    0, PETSC_NULL_INTEGER, ierr);CHKERRA(ierr)
            end if

            CALL MatGetOwnershipRange(M, RowStart, RowEnd, ierr);CHKERRA(ierr)
            nrows = RowEnd - RowStart

            allocate(irn_proc(nrows), col_proc(nrows))
            allocate(val_proc(nrows), val_proc_inv(nrows))

            do i = 1, nrows
                irn_proc(i) = RowStart + i - 1
                col_proc(i) = RowStart + i - 1
                val_proc(i) = volumes(RowStart+i) * one
                val_proc_inv(i) = 1d0/volumes(RowStart+i) * one
            end do

            do i = 1, nrows
                CALL MatSetValues(M, 1, irn_proc(i), 1, col_proc(i), val_proc(i), &
                              INSERT_VALUES, ierr);CHKERRA(ierr)
                CALL MatSetValues(Minv, 1, irn_proc(i), 1, col_proc(i), val_proc_inv(i), &
                              INSERT_VALUES, ierr);CHKERRA(ierr)
            end do

            CALL MatAssemblyBegin(M, MAT_FINAL_ASSEMBLY, ierr);CHKERRA(ierr)
            CALL MatAssemblyEnd(M, MAT_FINAL_ASSEMBLY, ierr);CHKERRA(ierr)

            CALL MatAssemblyBegin(Minv, MAT_FINAL_ASSEMBLY, ierr);CHKERRA(ierr)
            CALL MatAssemblyEnd(Minv, MAT_FINAL_ASSEMBLY, ierr);CHKERRA(ierr)

            ! call PetscViewerPushFormat(PETSC_VIEWER_STDOUT_WORLD, PETSC_VIEWER_ASCII_DENSE, ierr)
            ! call MatView(Minv, PETSC_VIEWER_STDOUT_WORLD, ierr)
            ! stop

        end subroutine mass_matrix
!! ======================================================================== !!
    subroutine solve(this, params)
        !! Pipeline of the EGV Solver.
        !! Saving Eigenvalues to file is done in only one processor!
        class(method), intent(inout) :: this
        class(paramlist), intent(in) :: params

        call MPI_Comm_rank(PETSC_COMM_WORLD, rank, ierr);CHKERRA(ierr)
        call preconditioning(this, params)
        call factor_matrix(this, params)
        call krylov_solver(this, params)
        if (rank.eq.0) call save_eig(this, params)
        call MPI_BARRIER(PETSC_COMM_WORLD, ierr);CHKERRA(ierr)

    end subroutine solve
!! ======================================================================== !!
    subroutine preconditioning(this, params)
        !! ONLY SHIFT&INVERT IMPLEMENTED SO FAR
        !! SPECIFIC SUBROUTINE CREATED TO ADD TYPES OF PRECONDITIONING
        class(method), intent(inout)  :: this
        class(paramlist), intent(in)  :: params
        character(len=250)            :: pout

        if (params%shift .NE. (0d0,0d0)) then
            ! if (rank .eq. 0) then
            !     write(*,*) 'Shift parameter to be used = ', real(this%shift), &
            !                                                 aimag(this%shift)
            !     write(*,*)
            ! end if
            write(pout,'(A, F8.4, F8.4, A)') "Shift parameter to be used =  ", &
                                real(params%shift), aimag(params%shift), '\n'
            call PetscPrintf(PETSC_COMM_WORLD, trim(pout), ierr)

            IF (params%generalized) THEN
                write(pout,'(a)') "|A - sigma*M|"
                call PetscPrintf(PETSC_COMM_WORLD, trim(pout), ierr)
                call MatAXPY(A, -params%shift, M, DIFFERENT_NONZERO_PATTERN, ierr);CHKERRA(ierr)
            ELSE
                if (params%scalejmatrix) then
                    write(pout,'(a)') '|Inv(M)*A - sigma*I|'
                else
                    write(pout,'(a)') '|A - sigma*I|'
                end if
                call PetscPrintf(PETSC_COMM_WORLD, trim(pout), ierr)
                ! Tell PETSc to go ahead with the allocatation of new matrix elements if needed
                call MatSetOption(A,MAT_NEW_NONZERO_ALLOCATION_ERR,PETSC_FALSE,ierr);CHKERRA(ierr)
                call MatShift(A, -params%shift, ierr);CHKERRA(ierr)
            END IF
        end if

        ! call PetscViewerPushFormat(PETSC_VIEWER_STDOUT_WORLD, PETSC_VIEWER_ASCII_DENSE, ierr)
        ! call MatView(A, PETSC_VIEWER_STDOUT_WORLD, ierr)
        ! stop

    end subroutine preconditioning
!! ======================================================================== !!
    subroutine factor_matrix(this, params)
        ! Call MUMPS/PETSc to factorize the matrix
        ! This has to be done only once
        ! TODO: Leave some pre-set up GMRES options

        class(method), intent(inout)  :: this
        class(paramlist), intent(in)  :: params
        !Vec                           :: u,x,b
        real(kind=RP)                 :: tol

        IF (this%islu) stop 'Factorization process has been already done'
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
!         Create the linear solver and set various options
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        call KSPCreate(PETSC_COMM_WORLD, ksp, ierr);CHKERRA(ierr)
        call KSPSetOperators(ksp, A, A, ierr);CHKERRA(ierr)
        tol = 1.e-7
        call KSPSetTolerances(ksp, tol, PETSC_DEFAULT_REAL, &
        &     PETSC_DEFAULT_REAL, PETSC_DEFAULT_INTEGER, ierr);CHKERRA(ierr)
        call KSPSetFromOptions(ksp, ierr);CHKERRA(ierr)

        if (params%factorization.eq.'GMRES') THEN
            call KSPSetType(ksp, KSPGMRES, ierr);CHKERRA(ierr)
            call KSPGMRESSetRestart(ksp, 30, ierr);CHKERRA(ierr)
        ! call KSPGMRESSetOrthogonalization(ksp, &
            ! KSPGMRESClassicalGramSchmidtOrthogonalization, ierr)

        else if (params%factorization.eq.'LU') THEN
            IF (rank.eq.0) then
                print*, ''
                print*, '************************'
                print*, '    LU decomposition    '
                print*, '************************'
                print*, ''
            END IF
            call KSPSetType(ksp, KSPPREONLY, ierr);CHKERRA(ierr)
            call KSPGetPC(ksp, pc, ierr);CHKERRA(ierr)
            call PCSetType(pc, PCLU, ierr);CHKERRA(ierr)
            call PCFactorSetMatSolverType(pc, MATSOLVERMUMPS, ierr);CHKERRA(ierr)
            CALL PcSetOperators(pc, A, A, ierr);CHKERRA(ierr)
            call PCFactorSetUpMatSolverType(pc, ierr);CHKERRA(ierr)
            call PCFactorGetMatrix(pc, F, ierr);CHKERRA(ierr)

            !! MUMPS SOLVER PARAMETERS !!
            CALL MatMumpsSetIcntl(F, 6, 7, ierr)   !!! LU Pre-Permutation
            CALL MatMumpsSetIcntl(F, 7, 7, ierr)   !!! Partitioning
            ! CALL MatMumpsSetIcntl(F, 8, 8, ierr)   !!! Scaling strategy
            ! CALL MatMumpsSetIcntl(F, 14, 350, ierr) !!  % Memory on workspace
            if (params%statistics) then
                CALL MatMumpsSetIcntl(F, 4,  2, ierr) ! Print linear solver statistics
            else
                CALL MatMumpsSetIcntl(F, 4,  1, ierr) ! Print only errors
            end if
            CALL MatMumpsSetIcntl(F, 11, 2, ierr) ! Statistics of linear solver
            ! CALL MatMumpsSetIcntl(F, 18, 3, ierr) ! strategy matrix distrib
            CALL MatMumpsSetIcntl(F, 10, 4, ierr) ! Max steps iter. refinement
            ! call MatMumpsSetIcntl(F, 24, 1, ierr) ! Threshhold for row pivot detection
            call MatMumpsSetIcntl(F, 33, 1, ierr) ! Compute determinant of A

            if (size.gt.1) then
                CALL MatMumpsSetIcntl(F, 10, 0, ierr)
                CALL MatMumpsSetIcntl(F, 11, 0, ierr)
            end if
            icntl = 3
            val = 1.e-6
            call MatMumpsSetCntl(F, icntl, val, ierr)
        end if

! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
!         Perform the factorization
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        call KSPSetFromOptions(ksp, ierr);CHKERRA(ierr)
        call KSPSetUp(ksp, ierr);CHKERRA(ierr)

! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
!       If LU was chosen, acquires extra info from MUMPS
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        IF (rank.eq.0) print*, 'DONE'
        if (params%factorization.eq.'LU') THEN
            icntl = 3
            call MatMumpsGetCntl(F, icntl, cntl, ierr)
            call MatMumpsGetInfog(F, 34, infog34, ierr)
            call MatMumpsGetRinfog(F, 12, rinfo12, ierr)
            call MatMumpsGetRinfog(F, 13, rinfo13, ierr)
            if (rank .eq. 0) then
                write(*,*)
                write(*,'(A,1pe11.2)') 'Mumps row pivot threshhold = ', cntl
                write(*,'(A,1pe11.2,A,1pe11.2,A,i8)') 'Mumps determinant=(', &
                            rinfo12,',',rinfo13,')*2^',infog34
                write(*,*)
            endif
        end if

        this%islu = .true.

! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
!        TEST FOR DEBUGGING PURPOSES
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
!         IF (SIZE.EQ.1) then
!             call VecCreateSeq(PETSC_COMM_WORLD,this%dimPrb,u,ierr);CHKERRA(ierr)
!         else IF (SIZE.GT.1) then
!             call VecCreateMPI(PETSC_COMM_WORLD,PETSC_DECIDE,this%dimPrb,u,ierr);CHKERRA(ierr)
!         end if
!         call VecDuplicate(u,b,ierr);CHKERRA(ierr)
!         call VecDuplicate(b,x,ierr);CHKERRA(ierr)
!
! !  Set exact solution; then compute right-hand-side vector.
!         one = 1.0d0
!         neg_one = -1.0d0
!         call VecSet(u,one,ierr);CHKERRA(ierr)
!         call MatMult(A,u,b,ierr);CHKERRA(ierr)
!
!         call KSPSolve(ksp,b,x,ierr);CHKERRA(ierr)
!
!         call KSPGetIterationNumber(ksp,its,ierr);CHKERRA(ierr)
!
! ! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
! !                     Check solution and clean up
! ! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
!         call VecAXPY(x,neg_one,u,ierr);CHKERRA(ierr)
!         call VecNorm(x,NORM_2,norm,ierr);CHKERRA(ierr)
!         call KSPGetIterationNumber(ksp,its,ierr);CHKERRA(ierr)
!
!         if (rank .eq. 0) then
!             if (norm .gt. 1.e-12) then
!                write(*,100) norm,its
!                write(*,*)
!             else
!                write(*,110) its
!                write(*,*)
!             endif
!         endif
! 100     format('Norm of error ',1pe11.4,' iterations ',i5)
! 110     format('Norm of error < 1.e-12,iterations ',i5)
!         call VecDestroy(u,ierr);CHKERRA(ierr)
!         call VecDestroy(x,ierr);CHKERRA(ierr)
!         call VecDestroy(b,ierr);CHKERRA(ierr)
        ! stop

    end subroutine factor_matrix
!! ======================================================================== !!
    subroutine krylov_solver(this, params)
        ! Eigenvalue solver selector (ARPACK/ARNOLDI)
        class(method), intent(inout) :: this
        class(paramlist), intent(in) :: params
        character(len=250)           :: pout

        write(pout,'(a)') "\n*******************************************\n"
        call PetscPrintf(PETSC_COMM_WORLD, trim(pout),ierr)

        if (params%use_arpack) then
            write(pout,'(a)') "Starting ARPACK routines...\n\n"
            call PetscPrintf(PETSC_COMM_WORLD, trim(pout),ierr)
            call arp_arnoldi(this, params)
        ELSE
            write(pout,'(a)') "Starting ARNOLDI method...\n\n"
            call PetscPrintf(PETSC_COMM_WORLD, trim(pout),ierr)
            call arnoldi_iteration(this, params)
        end if

    end subroutine krylov_solver
!! ======================================================================== !!
    subroutine arnoldi_iteration(this, params)
    ! Compute krylov subspace of the inverted matrix using Linear Solver
    ! to get V vector A⁻¹X=V -> AV=X
    ! PETSc is used to solve the linear system at each step of the algorithm.
    ! Local and Class arrays are used to store the results
    !
    ! TODO: This routine will end up dissapearing, as it has not implemented the
    !       M matrix for the generalized problem. And is also less accurate...

        class(method), intent(inout)   :: this
        type(paramlist), intent(in)    :: params
        integer                        :: i, j
        COMPLEX(kind=RP), allocatable  :: work(:), vl(:,:), rwork(:), vcol(:)
        real(kind=RP)                  :: v1, kx
        integer                        :: info
        integer                        :: which_ev
        complex(kind=RP), allocatable  :: EIGENVEC(:)
        complex(kind=RP)               :: egval
        real(kind=RP), allocatable     :: rd(:,:)
        complex(8), pointer            :: q_v(:), egf_v(:)
        character(len=250)             :: pout

        Vec                            :: q, x, egf, aux, aux2, qout
        VecScatter                     :: ctx
        PetscInt                       :: rstart, rend
        PetscScalar                    :: one

        CALL MPI_Comm_rank(PETSC_COMM_WORLD,rank,ierr)
        CALL MPI_Comm_size(PETSC_COMM_WORLD,size,ierr)

        one  = (1d0, 0d0)
        ! Allocate needed arrays
        allocate(this%V (this%dimPrb, params%ncv+1) )
        allocate(this%h (params%ncv+1, params%ncv+1) )
        allocate(vcol (this%dimPrb) )

        IF (SIZE.EQ.1) then
            call VecCreateSeq(PETSC_COMM_WORLD, this%dimPrb, q, ierr);CHKERRA(ierr)
        else IF (SIZE.GT.1) then
            call VecCreateMPI(PETSC_COMM_WORLD, PETSC_DECIDE, &
                                this%dimPrb, q, ierr);CHKERRA(ierr)
        end if

        call VecSet(q, one, ierr);CHKERRA(ierr)
        call VecDuplicate(q, x, ierr);CHKERRA(ierr)
        call VecDuplicate(q, egf, ierr);CHKERRA(ierr)
        call VecDuplicate(q, aux, ierr);CHKERRA(ierr)
        call VecDuplicate(q, aux2, ierr);CHKERRA(ierr)
        ! Alias class components var to simplify the code
        associate(m=>params%ncv, n=>this%dimPrb, v=>this%V, h=>this%h)
        !Init vectors
        call VecNorm(q, NORM_2, v1, ierr);CHKERRA(ierr)
        V(:,1) = one/v1
        h = (0d0,0d0)
        if (size.gt.1) call VecScatterCreateToZero(x, ctx, qout, ierr);CHKERRA(ierr)

        ! ------- ARNOLDI ITERATION (in a PETSc style) -----------
        do j = 1, m
            write(pout,'(A,i3,A,i3,A)') ' ITER: ',j, ' of ', m, '\n'
            call PetscPrintf(PETSC_COMM_WORLD, trim(pout),ierr)
            ! Assign J column of matrix V to Q vector
            call VecGetOwnershipRange(q, rstart, rend, ierr);CHKERRA(ierr)
            call VecGetArrayF90(q, q_v, ierr);CHKERRA(ierr)
            q_v = V(rstart+1:rend,j)
            call VecRestoreArrayF90(q, q_v, ierr);CHKERRA(ierr)

            ! Solve linear system Ax=q
            call KSPSolve(ksp, q, x, ierr);CHKERRA(ierr)
            ! Scatter vector Q to vector Qout, to perform algorithm step
            ! in sequential mode
            if (size.gt.1) then
                call VecScatterBegin(ctx, x, qout, INSERT_VALUES, &
                                        SCATTER_FORWARD, ierr);CHKERRA(ierr)
                call VecScatterEnd(ctx, x, qout, INSERT_VALUES, &
                                        SCATTER_FORWARD, ierr);CHKERRA(ierr)
                IF (rank.eq.0) THEN
                    call VecGetArrayF90(qout, q_v, ierr);CHKERRA(ierr)
                    do i = 1, j
                        vcol = V(:,i)
                        h(i,j) = dot_product(vcol, q_v)
                        q_v(:) = q_v(:) - h(i,j)*vcol
                    end do
                    call VecRestoreArrayF90(qout, q_v, ierr);CHKERRA(ierr)
                    call VecNorm(qout, NORM_2, v1, ierr);CHKERRA(ierr)
                    h(j+1,j) = v1
                    call VecGetArrayF90(qout, q_v, ierr);CHKERRA(ierr)
                    V(:,j+1) = q_v(:)/v1
                    call VecRestoreArrayF90(qout, q_v, ierr);CHKERRA(ierr)
                end if
                ! Broadcast matrices V and H and destroy the Scatter Object
                call MPI_Bcast(V, N*m+1, MPIU_SCALAR, 0, PETSC_COMM_WORLD, ierr);CHKERRA(ierr)

            else ! (size.lt.1)
                call VecCopy(x,q, ierr);CHKERRA(ierr)
                call VecGetArrayF90(q, q_v, ierr);CHKERRA(ierr)
                do i = 1, j
                    vcol = V(:,i)
                    h(i,j) = dot_product(vcol, q_v)
                    q_v(:) = q_v(:) - h(i,j)*vcol
                end do
                call VecRestoreArrayF90(q, q_v, ierr);CHKERRA(ierr)
                call VecNorm(q, NORM_2, v1, ierr);CHKERRA(ierr)
                h(j+1,j) = v1
                call VecGetArrayF90(q, q_v, ierr);CHKERRA(ierr)
                V(:,j+1) = q_v(:)/v1
                call VecRestoreArrayF90(q, q_v, ierr);CHKERRA(ierr)
            end if

        end do !j=1,this%m

        if(size.gt.1) then
            call VecScatterDestroy(ctx, ierr);CHKERRA(ierr)
            call VecDestroy(qout, ierr);CHKERRA(ierr)
        end if

        ! Allocate lapack arrays for eigenvalue computation
        allocate(vl(m+1,m+1), this%vr(m+1,m+1), this%eig(m+1))
        allocate(work(5*(m+1)), rwork(5*(m+1)), RD((m+1),4))
        rd = 0.0d0
        this%eig = 0.0d0

        ! Call LAPACK eigenvalue routine (serial routine)
        if (rank.eq.0) then
            write(*,*)
            write(*,*) 'SOLVING EGV!!'
            write(*,*)
            call zgeev('N', 'V', m+1, h, m+1, this%eig, vl, m+1, this%vr, m+1,&
                        work, 5*(m+1), rwork, info)
        end if
        if (size.gt.1) then
            call MPI_Bcast(this%VR, (m+1)*(m+1), MPIU_SCALAR, 0, PETSC_COMM_WORLD, ierr)
            call MPI_Bcast(this%eig, m+1, MPIU_SCALAR, 0, PETSC_COMM_WORLD, ierr)
        end if
        ! Calculate eigenvalue errors
        allocate(EIGENVEC(n))
        end associate
        eigenvec = 0.0d0
        do which_ev = 2, params%ncv+1
            ! Calculate Eigenmode and store it on EGF vector
            EIGENVEC = matmul(this%V, this%VR(:,which_ev))
            call VecGetOwnershipRange(egf, rstart, rend, ierr);CHKERRA(ierr)
            call VecGetArrayF90(egf, egf_v, ierr);CHKERRA(ierr)
            egf_v = EIGENVEC(rstart+1:rend)
            call VecRestoreArrayF90(egf, egf_v, ierr);CHKERRA(ierr)

            ! Correct EGV after shift and invert
            egval = 1.0d0/this%EIG(which_ev) + params%shift

            ! Calculate (Ax - kx)
            call MatMult(A_noshift, egf, aux, ierr);CHKERRA(ierr)
            call MatMult(M, egf, aux2, ierr);CHKERRA(ierr)
            call VecAXPY(aux, -egval, aux2, ierr);CHKERRA(ierr)

            rd(which_ev,1) = real(egval)
            rd(which_ev,2) = aimag(egval)
            call VecNorm(aux, NORM_2, rd(which_ev,3), ierr);CHKERRA(ierr)
            call VecScale(aux2, egval, ierr);CHKERRA(ierr)
            call VecNorm(aux2, NORM_2, kx, ierr);CHKERRA(ierr)
            rd(which_ev,4) = rd(which_ev,3) / kx
        end do
        ! %-----------------------------%
        ! | Display computed residuals. |
        ! %-----------------------------%
        if (rank.eq.0) then
            write(*,*)
            write(*,*) 'Error calculated as ||Ax - kx||'
            write(*,*) 'Relative error calculated as ||Ax - kx||/||kx||'
            call dmout(6, (params%ncv+1), 4, rd, (params%ncv+1), -6, &
                'Eigenvalues (Real, Imag), error and relative error')
        end if
        call MPI_BARRIER(PETSC_COMM_WORLD, ierr);CHKERRA(ierr)

        ! Clean up of arrays
        deallocate(vl, work, rwork, vcol)
        deallocate(EIGENVEC)

        call VecDestroy(q, ierr);CHKERRA(ierr)
        call VecDestroy(aux, ierr);CHKERRA(ierr)
        call VecDestroy(aux2, ierr);CHKERRA(ierr)
        call VecDestroy(egf, ierr);CHKERRA(ierr)
        call VecDestroy(x, ierr);CHKERRA(ierr)
        call MatDestroy(A, ierr);CHKERRA(ierr)
        call MatDestroy(A_noshift, ierr);CHKERRA(ierr)

    end subroutine arnoldi_iteration
! !! ======================================================================== !!
    subroutine arp_arnoldi(this, params)
        !! ARPACK Z-driver adapted to PETSc routines

        class(method), intent(inout) :: this
        class(paramlist), intent(in) :: params

        !%--------------%
        !| Local Arrays |
        !%--------------%
        Vec                                   :: vb, vx, vout, vz, vbksp
        Vec                                   :: aux, aux2, egf
        VecScatter                            :: ctx
        PetscInt                              :: rstart, rend
        COMPLEX(kind=RP), POINTER             :: vb_v(:), vx_v(:)
        COMPLEX(kind=RP), POINTER             :: egf_v(:), vout_v(:)
        COMPLEX(kind=RP), POINTER             :: Y(:), b(:), z(:)
        COMPLEX(KIND=RP), ALLOCATABLE, TARGET :: WORKD(:)
        COMPLEX(KIND=RP), ALLOCATABLE         :: WORKL(:), D(:), WORKEV(:)
        COMPLEX(KIND=RP), ALLOCATABLE         :: RESID(:), Y_aux(:)
        REAL(kind=RP), ALLOCATABLE            :: RWORK(:)
        REAL(kind=RP), ALLOCATABLE            :: rd(:,:)
        REAL(kind=RP)                         :: kx
        INTEGER                               :: IPARAM(11), IPNTR(14)
        LOGICAL, ALLOCATABLE                  :: select(:)

        !%---------------%
        !| Local Scalars |
        !%---------------%
        character(len=1)   :: bmat
        character(len=250) :: pout
        INTEGER            :: IDO, lWORKL, kinfo, i, j, ARERR, &
                              iter, NCONV, maxitr, ishfts, mode, istat

        external zcopy

        !%------------%
        !| Parameters |
        !%------------%
        complex(kind=RP) :: one, zero
        one  = (1.0d0, 0.0d0)
        zero = (0.0d0, 0.0d0)

        CALL MPI_Comm_rank(PETSC_COMM_WORLD, rank, ierr);CHKERRA(ierr)
        CALL MPI_Comm_size(PETSC_COMM_WORLD, size, ierr);CHKERRA(ierr)

        associate(NEIG=>params%nev, NCV=>params%ncv, tol=>params%tol, &
                    N=>this%dimPrb, which=>params%which)

        ! DIMENSION OF MATRIX AND NUMBER OF EGV IN KRYLOV SUBSPACE
    !       %----------------------------------------------------%
    !       | The number N is the dimension of the matrix. A     |
    !       | generalized eigenvalue problem is solved (BMAT =   |
    !       | 'G'.) NEV is the number of eigenvalues to be       |
    !       | approximated.  The user can modify NEV, NCV, WHICH |
    !       | to solve problems of different sizes, and to get   |
    !       | different parts of the spectrum.  However, The     |
    !       | following conditions must be satisfied:            |
    !       |                     N <= MAXN,                     |
    !       |                   NEV <= MAXNEV,                   |
    !       |               NEV + 1 <= NCV <= MAXNCV             |
    !       %----------------------------------------------------%
        ! Eigen System Lanczos Vectors
        ! Recommended value for NCV = 3*NEIG + 1
        lWORKL = 3*NCV**2 + 6*NCV
        ALLOCATE( WORKL(lWORKL), D(1:NEIG), Y_aux(N), &
                  WORKEV(3*NCV), SELECT(NCV), WORKD(3*N), &
                  RESID(N), this%V(N,NCV), this%eig(NCV), &
                  RWORK(NCV), RD(NCV,4), STAT=istat )

        IF ( istat /= 0 ) THEN
            stop 'FATAL ERROR. Memory allocation error.'
        END IF

        ! Generation of PETSc Vectors
        IF (SIZE.EQ.1) then
            write(pout,'(a)') ' Using ARPACK Seq Vectors!\n'
            call PetscPrintf(PETSC_COMM_WORLD, trim(pout), ierr)
            call VecCreateSeq(PETSC_COMM_WORLD, N, vb, ierr);CHKERRA(ierr)

        else IF (SIZE.GT.1) then
            write(pout,'(a)') ' Using ARPACK Parallel Vectors!\n'
            call PetscPrintf(PETSC_COMM_WORLD, trim(pout), ierr)
            call VecCreateMPI(PETSC_COMM_WORLD, PETSC_DECIDE, &
                                N, vb, ierr);CHKERRA(ierr)
        end if
        call VecDuplicate(vb, vx, ierr);CHKERRA(ierr)
        call VecDuplicate(vb, vbksp, ierr);CHKERRA(ierr)
        call VecDuplicate(vb, vz, ierr);CHKERRA(ierr)
        call VecDuplicate(vb, egf, ierr);CHKERRA(ierr)
        call VecDuplicate(vb, aux, ierr);CHKERRA(ierr)
        call VecDuplicate(vb, aux2, ierr);CHKERRA(ierr)

    !       %--------------------------------------------------%
    !       | The work array WORKL is used in ZNAUPD as        |
    !       | workspace.  Its dimension LWORKL is set as       |
    !       | illustrated above.  The parameter TOL determines |
    !       | the stopping criterion.  If TOL<=0, machine      |
    !       | precision is used.  The variable IDO is used for |
    !       | reverse communication and is initially set to 0. |
    !       | Setting INFO=0 indicates that a random vector is |
    !       | generated in ZNAUPD to start the Arnoldi         |
    !       | iteration.                                       |
    !       %--------------------------------------------------%

    !       %---------------------------------------------------%
    !       | This program uses exact shifts with respect to    |
    !       | the current Hessenberg matrix (IPARAM(1) = 1).    |
    !       | IPARAM(3) specifies the maximum number of Arnoldi |
    !       | iterations allowed.                               |
    !       %---------------------------------------------------%

        ! INITIALIZE VALUES. For details, see the documentation in ZNAUPD
        this%V(:,:) = zero
        this%eig = zero
        resid = zero
        Y_aux = zero
        workd = zero
        D = zero
        IDO    = 0
        kinfo  = 0
        maxitr = 1000
        ishfts = 1

        IF (params%generalized) THEN
            BMAT  = 'G' !! Solve a Standard Eigenvalue Problem
        ELSE
            BMAT  = 'I' !! Solve a Standard Eigenvalue Problem
        END IF
        mode = 3    !! Solve always with at least, INVERT (and shift of there is any)

        IPARAM = 0
        IPARAM(1) = ishfts
        IPARAM(3) = maxitr
        IPARAM(7) = mode

    !       %-------------------------------------------%
    !       | M A I N   L O O P (Reverse communication) |
    !       %-------------------------------------------%
        iter = 1
        write(pout,'(a)') '\n********************************************\n'
        call PetscPrintf(PETSC_COMM_WORLD, trim(pout),ierr)
        write(pout,*) 'Starting MAIN LOOP with MODE = ', mode, '\n'
        call PetscPrintf(PETSC_COMM_WORLD, trim(pout),ierr)
        write(pout,'(a)') '********************************************\n'
        call PetscPrintf(PETSC_COMM_WORLD, trim(pout),ierr)

        if (size.gt.1) call VecScatterCreateToZero(vx, ctx, vout, ierr);CHKERRA(ierr)

        DO WHILE( ido /= 99 )
    !           %---------------------------------------------%
    !           | Repeatedly call the routine DSAUPD and take |
    !           | actions indicated by parameter IDO until    |
    !           | either convergence is indicated or maxitr   |
    !           | has been exceeded.                          |
    !           | ZNAUP routine called in serial. Results are |
    !           | broadcasted afterwards.                     |
    !           %---------------------------------------------%
            if (rank.eq.0) then
                CALL ZNAUPD (ido, BMAT, N, which, NEIG, TOL, &
                    RESID, NCV, this%V, N, IPARAM, IPNTR, WORKD, WORKL, lWORKL, &
                    RWORK, kinfo )
            end if

            if (size.gt.1) then
                call MPI_Bcast(ido, 1, MPI_INT, 0, PETSC_COMM_WORLD, ierr);CHKERRA(ierr)
                call MPI_Bcast(IPNTR, 14, MPI_INT, 0, PETSC_COMM_WORLD, ierr);CHKERRA(ierr)
                call MPI_Bcast(WORKD, 3*N, MPIU_SCALAR, 0, PETSC_COMM_WORLD, ierr);CHKERRA(ierr)
                call MPI_Bcast(kinfo, 1, MPI_INT, 0, PETSC_COMM_WORLD, ierr);CHKERRA(ierr)
            end if

            IF (ido == -1) THEN
!---------------------------------------------------------------------
!               Perform  y <--- OP*x = inv[A-SIGMA*M]*M*x
!               Linear system solver that takes workd(ipntr(1)) as input,
!               returning the result to workd(inpntr(2))
!---------------------------------------------------------------------
                Y => workd(ipntr(2):ipntr(2)+n-1)
                b => workd(ipntr(1):ipntr(1)+n-1)

                call VecGetOwnershipRange(vb,rstart,rend,ierr);CHKERRA(ierr)
                call VecGetArrayF90(vb, vb_v, ierr);CHKERRA(ierr)
                vb_v(:) = b(rstart+1:rend)
                call VecRestoreArrayF90(vb, vb_v, ierr);CHKERRA(ierr)
                call MatMult(M, vb, vbksp, ierr);CHKERRA(ierr)

                call KSPSolve(ksp, vbksp, vx, ierr);CHKERRA(ierr)

                if (size.eq.1) then
                    call VecGetArrayF90(vx, vx_v, ierr);CHKERRA(ierr)
                    Y(:) = vx_v(:)
                    call VecRestoreArrayF90(vx, vx_v, ierr);CHKERRA(ierr)
                else
                    call VecScatterBegin(ctx, vx, vout, INSERT_VALUES, SCATTER_FORWARD, ierr);CHKERRA(ierr)
                    call VecScatterEnd(ctx, vx, vout, INSERT_VALUES, SCATTER_FORWARD, ierr);CHKERRA(ierr)
                    call VecGetArrayReadF90(vout, vout_v, ierr);CHKERRA(ierr)
                    if (rank.eq.0) Y(:) = vout_v(:)
                    call VecRestoreArrayReadF90(vout, vout_v, ierr);CHKERRA(ierr)
                end if

            ELSE IF (ido == 1) then
                if (rank.eq.0) then
                    write(*,*)
                    write(*,*) '*************************************************************'
                    write(*,*)
                    WRITE(*,'(A,I0)') 'Arpack reverse communication calls: ', Iter
                    ! write(*,*) 'IDO = ', IDO
                    write(*,*) '----------------------------------------------'
                    iter = iter + 1
                end if
!---------------------------------------------------------------------
!               Perform  y <--- OP*x = inv[A-SIGMA*M]*M*x
!               Linear system solver that takes workd(ipntr(1)) as input,
!               returning the result to workd(inpntr(2))
!---------------------------------------------------------------------
                Y => workd(ipntr(2):ipntr(2)+n-1)
                z => workd(ipntr(3):ipntr(3)+n-1)

                ! Compute Z = B*X
                call VecGetOwnershipRange(vz, rstart, rend, ierr);CHKERRA(ierr)
                call VecGetArrayF90(vz, vb_v, ierr);CHKERRA(ierr)
                vb_v(:) = z(rstart+1:rend)
                call VecRestoreArrayF90(vz, vb_v, ierr);CHKERRA(ierr)

                ! call MatMult(M, vz, vbksp, ierr);CHKERRA(ierr)
                call KSPSolve(ksp, vz, vx, ierr);CHKERRA(ierr)

                if (size.eq.1) then
                    call VecGetArrayF90(vx, vx_v, ierr);CHKERRA(ierr)
                    Y(:) = vx_v(:)
                    call VecRestoreArrayF90(vx, vx_v, ierr);CHKERRA(ierr)
                else
                    call VecScatterBegin(ctx,vx,vout,INSERT_VALUES,SCATTER_FORWARD, ierr);CHKERRA(ierr)
                    call VecScatterEnd(ctx,vx,vout,INSERT_VALUES,SCATTER_FORWARD, ierr);CHKERRA(ierr)
                    call VecGetArrayReadF90(vout, vout_v, ierr);CHKERRA(ierr)
                    if (rank.eq.0) Y(:) = vout_v(:)
                    call VecRestoreArrayReadF90(vout, vout_v, ierr);CHKERRA(ierr)
                end if


            ELSE IF (ido == 2) then
                Y => workd(ipntr(2):ipntr(2)+n-1)
                b => workd(ipntr(1):ipntr(1)+n-1)
                call VecGetOwnershipRange(vb, rstart, rend, ierr);CHKERRA(ierr)
                call VecGetArrayF90(vb, vb_v, ierr);CHKERRA(ierr)
                vb_v = b(rstart+1:rend)
                call VecRestoreArrayF90(vb, vb_v, ierr);CHKERRA(ierr)
                call MatMult(M, vb, vx, ierr);CHKERRA(ierr)

                if (size.eq.1) then
                    call VecGetOwnershipRange(vx, rstart, rend, ierr);CHKERRA(ierr)
                    call VecGetArrayF90(vx, vx_v, ierr);CHKERRA(ierr)
                    Y(:) = vx_v(:)
                    call VecRestoreArrayF90(vx, vx_v, ierr);CHKERRA(ierr)
                else
                    call VecScatterBegin(ctx,vx,vout,INSERT_VALUES,SCATTER_FORWARD, ierr);CHKERRA(ierr)
                    call VecScatterEnd(ctx,vx,vout,INSERT_VALUES,SCATTER_FORWARD, ierr);CHKERRA(ierr)
                    call VecGetArrayReadF90(vout, vout_v, ierr);CHKERRA(ierr)
                    if (rank.eq.0) Y(:) = vout_v(:)
                    call VecRestoreArrayReadF90(vout, vout_v, ierr);CHKERRA(ierr)
                end if

            END IF !(IDO SELECTOR)

        END DO

        if (kinfo /= 0) then
    !           %--------------------------%
    !           | Error message, check the |
    !           | documentation in DNAUPD  |
    !           %--------------------------%
            if (rank.eq.0) then
                WRITE(*,*) 'Error with ZNAUPD, info = ', kinfo
                WRITE(*,*) 'Error during the IRAM process. Stopping...'
                stop
            end if
        ELSE
            if (rank.eq.0) then
                write(*,*)
                write(*,*) 'ZNAUPD terminated. Number of calls = ', IPARAM(3)
                write(*,*) 'SOLVING EGV!!'
                write(*,*)
    !           %-------------------------------------------%
    !           | No fatal errors occurred.                 |
    !           | Post-Process using DSEUPD.                |
    !           |                                           |
    !           | Computed eigenvalues may be extracted.    |
    !           |                                           |
    !           | Eigenvectors may also be computed now if  |
    !           | desired.  (indicated by rvec = .true.)    |
    !           %-------------------------------------------%
                CALL ZNEUPD ( .TRUE., 'A', select, D, &
                    this%V, N, params%shift, WORKEV, BMAT, N, &
                    which, NEIG, TOL, RESID, NCV, this%V, N, &
                    IPARAM, IPNTR, WORKD, WORKL, lWORKL, RWORK, ARERR )
                write(*,*) 'ZNEUPD terminated.'
            end if
    !           %----------------------------------------------%
    !           | Eigenvalues are returned in the First column |
    !           | of the one dimensional array D and the       |
    !           | corresponding eigenvectors are returned in   |
    !           | the First NEV columns of the two dimensional |
    !           | array V if requested.  Otherwise, an         |
    !           | orthogonal basis for the invariant subspace  |
    !           | corresponding to the eigenvalues in D is     |
    !           | returned in V.                               |
    !           %----------------------------------------------%
            if (size.gt.1) then
                call MPI_Bcast(ARERR, 1, MPI_INT, 0, PETSC_COMM_WORLD, ierr);CHKERRA(ierr)
                call MPI_Bcast(D, NEIG, MPIU_SCALAR, 0, PETSC_COMM_WORLD, ierr);CHKERRA(ierr)
                call MPI_Bcast(IPARAM, 11, MPI_INT, 0, PETSC_COMM_WORLD, ierr);CHKERRA(ierr)
                call MPI_Bcast(this%V, N*NCV, MPIU_SCALAR, 0, PETSC_COMM_WORLD, ierr);CHKERRA(ierr)
                call MPI_Bcast(RESID, N, MPIU_SCALAR, 0, PETSC_COMM_WORLD, ierr);CHKERRA(ierr)
            end if

            IF (ARERR /= 0) THEN
                WRITE(*,*) ' Error with ZNEUPD, info = ', ARERR
                WRITE(*,*) ' Error during eigenvalue solving. Stopping...'
                stop
            ELSE
                nconv = iparam(5)

                DO i=1,NEIG
                    this%eig(i) = D(i)
                END DO
                DO j=1, nconv
                    ! %---------------------------%
                    ! | Compute the residual norm |
                    ! |                           |
                    ! |  ||A*x - k*M*x||/||k*x||  |
                    ! |                           |
                    ! | for the NCONV accurately  |
                    ! | computed eigenvalues and  |
                    ! | eigenvectors.  (iparam(5) |
                    ! | indicates how many are    |
                    ! | accurate to the requested |
                    ! | tolerance)                |
                    ! %---------------------------%
                    call VecGetOwnershipRange(egf, rstart, rend,ierr);CHKERRA(ierr)
                    call VecGetArrayF90(egf, egf_v, ierr);CHKERRA(ierr)
                    egf_v = this%v(rstart+1:rend,j)
                    call VecRestoreArrayF90(egf, egf_v, ierr);CHKERRA(ierr)

                    call MatMult(A_noshift, egf, aux, ierr);CHKERRA(ierr)
                    call MatMult(M, egf, aux2, ierr);CHKERRA(ierr)
                    call VecAXPY(aux, -d(j), aux2, ierr);CHKERRA(ierr)

                    rd(j,1) = real (d(j))
                    rd(j,2) = aimag (d(j))
                    call VecNorm(aux, NORM_2, rd(j,3), ierr);CHKERRA(ierr)
                    call VecScale(aux2, d(j), ierr);CHKERRA(ierr)
                    call VecNorm(aux2, NORM_2, kx, ierr);CHKERRA(ierr)
                    rd(j,4) = rd(j,3) / kx
                end do

                ! %-----------------------------%
                ! | Display computed residuals. |
                ! %-----------------------------%
                if (rank.eq.0) then
                    print*, 'Error calculated as ||Ax - kx||'
                    print*, 'Relative error calculated as ||Ax - kx||/||kx||'
                    call dmout(6, nconv, 4, rd, ncv, -6, &
                        'Eigenvalues (Real, Imag), error and relative error')
                end if

            END IF

            ! %-------------------------------------------%
            ! | Print additional convergence information. |
            ! %-------------------------------------------%
            if (rank.eq.0) then
                if ( kinfo == 1) then
                    print *, ' '
                    print *, ' Maximum number of iterations reached.'
                    print *, ' '
                else if ( kinfo == 3) then
                    print *, ' '
                    print *, ' No shifts could be applied during implicit', &
                             ' Arnoldi update, try increasing NCV.'
                    print *, ' '
                end if

                write(*,*) ' '
                write(*,*) 'DRIVER INFORMATION: '
                write(*,*) '=================== '
                write(*,*) ' '
                write(*,'(A,I10)') ' Size of the matrix is ', n
                write(*,'(A,I4)') ' The number of Ritz values requested is ', NEIG
                write(*,'(A,A,I4)') ' The number of Arnoldi vectors generated', &
                         ' (NCV) is ', ncv
                write(*,*) ' What portion of the spectrum: ', which
                write(*,'(A,I4)') ' The number of converged Ritz values is ', &
                         nconv
                write(*,'(A,A,I3)') ' The number of Implicit Arnoldi update', &
                         ' iterations taken is ', iparam(3)
                write(*,'(A,I3)') ' The number of OP*x is ', iparam(9)
                write(*,'(A,ES10.2)') ' The convergence criterion is ', tol
                write(*,*) ' '
            end if

        END IF

        end associate

        ! Clean up...
        if (allocated(workl)) deallocate(workl)
        if (allocated(D)) deallocate(D)
        if (allocated(workev)) deallocate(workev)
        if (allocated(select)) deallocate(select)
        if (allocated(workd)) deallocate(workd)
        if (allocated(resid)) deallocate(resid)
        if (allocated(rwork)) deallocate(rwork)
        if (allocated(rd)) deallocate(rd)

        ! Destroy the instance (deallocate internal data structures)
        call VecDestroy(vb, ierr);CHKERRA(ierr)
        call VecDestroy(vz,ierr)
        call VecDestroy(vbksp,ierr)
        call VecDestroy(vx, ierr);CHKERRA(ierr)
        call VecDestroy(egf, ierr);CHKERRA(ierr)
        call VecDestroy(aux, ierr);CHKERRA(ierr)
        call VecDestroy(aux2, ierr);CHKERRA(ierr)
        call MatDestroy(A, ierr);CHKERRA(ierr)
        call MatDestroy(A_noshift, ierr);CHKERRA(ierr)
        call MatDestroy(M, ierr);CHKERRA(ierr)
        call MatDestroy(Minv, ierr);CHKERRA(ierr)
        IF (size.gt.1) then
            call VecScatterDestroy(ctx, ierr)
            call VecDestroy(vout, ierr)
        END IF

        call MPI_BARRIER(PETSC_COMM_WORLD, ierr);CHKERRA(ierr)

    end subroutine arp_arnoldi
! !! ======================================================================== !!
    subroutine save_eig(this, params)
        !! Save eigenvalues to text file.
        class(method),    intent(inout) :: this
        class(paramlist), intent(in)    :: params
        integer                         :: fh, i

        write(*,*) 'SAVING EIGENVALUES IN: ', trim(params%eigv_file)
        open(newunit=fh, file=trim(params%eigv_file), action='WRITE')

        if (params%use_arpack) then
            do i = 1, params%nev
                write(fh, '(I0,3x,g0,3x,g0)') i, real(this%eig(i)), &
                                                 aimag(this%eig(i))
            end do
        else
            ! First eigenvalue is null and will crash when inverting, so it's ommitted
            write(fh, '(I0,3x,g0,3x,g0)') 1, 0.0, 0.0
            DO i = 2, params%ncv+1
                write(fh, '(I0,3x,g0,3x,g0)') i, real(1d0/this%EIG(i) + params%shift), &
                                                 aimag(1d0/this%EIG(i) + params%shift)
            END DO
        end if
        close(fh)

        WRITE(*,*) '   DONE'; write(*,*)

    end subroutine
! !! ======================================================================== !!
    subroutine save_hk(this, matrix, params)
        ! Save information for restart in unformatted format.
        class(method),    intent(inout) :: this
        class(amatrix),   intent(in)    :: matrix
        class(paramlist), intent(in)    :: params
        integer :: fh

        ! Write EGVals and Krylov matrix to an unformatted file
        open(newunit=fh, file=trim(params%hkoutfile), form='UNFORMATTED', &
                access='STREAM',action='WRITE')
        write(fh) matrix%nvar
        write(fh) matrix%ndim
        write(fh) matrix%neq
        write(fh) params%ncv
        write(fh) this%eig
        write(fh) this%v
        close(fh)
    end subroutine
!! ======================================================================== !!
    subroutine destroy(this)
        class(method), intent(inout) :: this
        deallocate(this%v, this%eig)
        if (allocated(this%vr)) deallocate(this%vr)
    end subroutine destroy
!! ======================================================================== !!
    subroutine load_hk(this, matrix, params)
        ! Load restart information from binary (unformatted) file.
        class(method), intent(inout)    :: this
        class(amatrix),intent(inout)    :: matrix
        class(paramlist), intent(inout) :: params
        integer                         :: fh, ncv

        ! Read EGVals and Krylov matrix from an unformatted file
        open(newunit=fh, file=trim(params%hkinfile), form='UNFORMATTED', &
                access='STREAM',action='READ')
        read(fh) matrix%nvar
        read(fh) matrix%ndim
        read(fh) matrix%neq
        read(fh) ncv
        if (ncv .ne. params%ncv) THEN
            write(*,*) 'NCV has been corrected with the data from restart file.'
            write(*,*) 'NCV = ', ncv
            params%ncv = ncv
        end if
        ! Allocate Krylov matrix
        if (allocated(this%eig)) deallocate(this%eig)
        allocate(this%eig(params%ncv))
        if (allocated(this%v)) deallocate(this%v)
        allocate( this%V(matrix%nvar, params%ncv) )
        read(fh) this%eig
        read(fh) this%v
        close(fh)
    end subroutine
!! ======================================================================== !!
end module egv_solver
