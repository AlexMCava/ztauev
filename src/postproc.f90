module postprocess
    use precision_mod,   only: RP
    use paramlistClass
    use jac_matrix,      only: amatrix
    use egv_solver,      only: method
    use math_tools
    use eigvec2plt,      only: save2plt
    use eigvec2pval,     only: savepval
    implicit none

CONTAINS
!! ======================================================================== !!
    subroutine save_eigvec(which_eigvec, params, jacobian, solver, eigvec, error)
        !! Main pipeline for eigenmode calculation and storing.
        !! Eigenmodes can be stored in either TecPlot or NetCDF format.
        integer,                       intent(in)  :: which_eigvec
        class(amatrix),                intent(in)  :: jacobian
        class(paramlist),              intent(in)  :: params
        class(method),                 intent(in)  :: solver
        complex(kind=RP), allocatable, intent(out) :: eigvec(:,:), error(:,:)
        character(len=250)                         :: eigvecfile

        write(eigvecfile,'(I0)') which_eigvec
        call compute_eigvec(which_eigvec, jacobian, params, solver, eigvec, error)

        eigvecfile = trim(params%eigf_path)//'_DIR_'//trim(eigvecfile)
        if (params%spval) then
            eigvecfile = trim(eigvecfile)//'.pval'
            call savepval(params%dualgrid, eigvecfile, eigvec, error, &
                            jacobian%ndim, jacobian%neq, jacobian%localid)
        else
            eigvecfile = trim(eigvecfile)//'.dat'
            call save2plt(eigvecfile, eigvec, error, jacobian%coord)
        end if

    end subroutine save_eigvec
!! ======================================================================== !!
    subroutine compute_eigvec(which_ev, matrix, params, solver, eigenvec, egverror)
        !! Eigenmode calculation. Depending on the algorithm used (standard
        !! Arnoldi or ARPACK), different procedures are used.
        integer,                       intent(in)     :: which_ev
        class(amatrix),                intent(in)     :: matrix
        class(paramlist),              intent(in)     :: params
        class(method),                 intent(in)     :: solver
        complex(kind=RP), allocatable, intent(out)    :: eigenvec(:,:)
        complex(kind=RP), allocatable, intent(out)    :: egverror(:,:)

        complex(kind=RP), allocatable :: aux(:), auxvec(:)
        complex(kind=RP)              :: egval
        integer                       :: point, eq, npoint, var, i

        npoint = matrix%nvar/matrix%neq

        ! Allocate eigenfunction arrays
        if (allocated(eigenvec)) deallocate(eigenvec)
        allocate (eigenvec(npoint,matrix%neq))
        if (allocated(aux)) deallocate(aux)
        allocate (aux(matrix%nvar))
        if (allocated(egverror)) deallocate(egverror)
        allocate (egverror(npoint,matrix%neq))

        ! Eigenvector error reconstruction
        allocate(auxvec(matrix%nvar))
        if (params%use_arpack) then
            egval = solver%eig(which_ev)
            auxvec = solver%v(:,which_ev)
        else
            egval = 1.0d0/solver%EIG(which_ev) + params%shift
            auxvec = matmul(solver%V, solver%VR(:,which_ev))
        end if
        call zamux(matrix%nvar, auxvec, aux, &
                    solver%data, solver%col_ind, solver%row_ptr)
        if (params%generalized) then
            do point = 1, matrix%nvar
                auxvec(point) = auxvec(point) * matrix%vol(point)
            end do
        end if
        call zaxpy (matrix%nvar, -egval, auxvec, 1, aux, 1)
        deallocate(auxvec)

        ! Eigenvector reconstruction
        eigenvec = 0.0_RP
        egverror = 0.0_RP
        do eq = 1, matrix%neq
            do point = 1, npoint
                var = (point - 1) * matrix%neq + eq
                if (params%use_arpack) then
                    eigenvec(point,eq) = eigenvec(point,eq) + solver%v(var,which_ev)
                ELSE
                    do i = 1, params%ncv+1
                        eigenvec(point,eq) = eigenvec(point,eq) + &
                                            solver%v(var,i)*solver%vr(i,which_ev)
                    end do
                end if
                egverror(point,eq) = egverror(point,eq) + aux(var)
            end do
        end do
        deallocate(aux)
    end subroutine compute_eigvec
!! ======================================================================== !!
end module postprocess
