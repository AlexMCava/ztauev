############################################################################
##        Makefile for zTAUev software
##
##        Adjust only what is needed!
##
##        Author: Alejandro Martínez-Cava (UPM)
##        Last revision: 29 Jan 2018
############################################################################
.DEFAULT_GOAL := all

INC_DIR = include
BUILD_DIR = build
BIN_DIR = bin
SRC_DIR = src
EXECUTABLE = zTAUev

include Make.inc
include ${PETSC_DIR}/lib/petsc/conf/variables
include ${PETSC_DIR}/lib/petsc/conf/rules

OBJS = precision \
       read_NetCDF \
	   math_tools \
	   jac_tools \
	   paramlistClass \
	   jac_matrix \
	   egvsolverPETSC \
	   eigvec2VTK \
	   eigvec2pval \
	   postproc \
	   main

OBJS_BUILD = ${foreach obj, ${OBJS}, ${BUILD_DIR}/${obj}.o}

LIBS_ALL = ${LIB_PETSC} ${LIB_BLAS_LAPACK} ${LIB_ARPACK} ${LIB_MPI} ${LIB_NETCDFF}
INCLUDE = ${PETSC_INC} ${NETCDF_INC} ${MPI_INC}

FFLAGS = -cpp -O3 -g -ftree-vectorize -flto -ftree-vectorizer-verbose=1 \
			-Wtabs -fopenmp

all: distclean ${EXECUTABLE}

distclean:
	${RM} -rf ${BIN_DIR}/* ${BUILD_DIR}/*.o ${INC_DIR}/*.mod

${OBJS}: %:
	${FLINKER} -I${INC_DIR} -J${INC_DIR} ${INCLUDE} \
	 			-c ${SRC_DIR}/$@.f90 -o ${BUILD_DIR}/$@.o

${EXECUTABLE}: ${OBJS}
	${FLINKER} ${INCLUDE} ${LIBS_ALL} \
	    -o ${BIN_DIR}/${EXECUTABLE} ${OBJS_BUILD} ${LIBS_ALL} ${PETSC_KSP_LIB}
