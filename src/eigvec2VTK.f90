module eigvec2plt
    use precision_mod,   only: RP
    implicit none
contains
!! ======================================================================== !!
    subroutine save2plt(file, eigvec, error, coord)
        !! Save eigenmodes to TecPlot ASCII format.
        !! Ordered by POINTS, will give SCATTER data!
        !! Useful for debugging, Domain-Reduction, and later post-processing.
        character(len=*),   intent(in) :: file
        COMPLEX(kind = RP), intent(in) :: eigvec(1:,1:), error(1:,1:)
        real(kind = RP),    intent(in) :: coord(1:,1:)

        integer  :: nvar, ndim, npoint, fd, i

        npoint = ubound(eigvec, dim=1)
        nvar   = ubound(eigvec, dim=2)
        ndim   = ubound(coord,  dim=2)
        !print*, 'npoint, ndim, nvar = ', npoint, ndim, nvar
        open(newunit=fd, file=trim(file), action='WRITE')

        if(ndim == 2) then
            if (nvar == 4) then  ! 2D-Laminar flow
                write(fd,*) 'TITLE = "TAU"'
                write(fd,*) 'VARIABLES = "X" "Z" "Rho_r" "RhoU_r" "RhoW_r" "RhoE_r" ', &
                            '"Rho_i" "RhoU_i" "RhoW_i" "RhoE_i" ', &
                            '"E_Rho_r" "E_RhoU_r" "E_RhoW_r" "E_RhoE_r"'
                write(fd,*) 'ZONE T = "FP"'
                write(fd,*) 'I=', npoint, ' J=1 K=1 ZONETYPE=Ordered'
                write(fd,*) 'DATAPACKING = POINT'
                do i=1,npoint
                    write(fd,'(14(2x,1pg0))') coord(i,1:ndim), &
                                                real(eigvec(i,1:nvar)), &
                                                aimag(eigvec(i,1:nvar)), &
                                                real(error(i,1:nvar))
                end do
            else if (nvar == 5) then  ! 2D-Turb 1eq
                write(fd,*) 'TITLE = "TAU"'
                write(fd,*) 'VARIABLES = "X" "Z" "Rho" "RhoU" "RhoW" "RhoE" "Turb"', &
                            '"Rho_i" "RhoU_i" "RhoW_i" "RhoE_i" "Turb_i" ', &
                            '"E_rho" "E_rhoU" "E_rhoW" "E_rhoE" "E_T1"'
                write(fd,*) 'ZONE T = "FP"'
                write(fd,*) 'I=',npoint,' J=1 K=1 ZONETYPE=Ordered'
                write(fd,*) 'DATAPACKING = POINT'
                do i=1,npoint
                    write(fd,'(17(2x,1pg0))') coord(i,1:ndim), &
                                                real(eigvec(i,1:nvar)), &
                                                aimag(eigvec(i,1:nvar)), &
                                                real(error(i,1:nvar))
                end do

            else if (nvar == 6) then ! 2D-Turb 2eq
                write(fd,*) 'TITLE = "TAU"'
                write(fd,*) 'VARIABLES = "X" "Z" "Rho_r" "RhoU_r" "RhoW_r" "RhoE_r"', &
                            ' "Turb1_r" "Turb2_r" ', &
                            '"Rho_i" "RhoU_i" "RhoW_i" "RhoE_i " ', &
                            '"Turb1_i" "Turb2_i" ', &
                            '"E_rho" "E_rhoU" "E_rhoW" "E_rhoE" "E_T1" "E_T2"'
                write(fd,*) 'ZONE T = "FP"'
                write(fd,*) 'I=',npoint,' J=1 K=1 ZONETYPE=Ordered'
                write(fd,*) 'DATAPACKING = POINT'
                do i=1,npoint
                    write(fd,'(20(2x,1pg0))') coord(i,1:ndim), &
                                                real(eigvec(i,1:nvar)), &
                                                aimag(eigvec(i,1:nvar)), &
                                                real(error(i,1:nvar))
                enddo

            else
                write(*,*) '   More than 6 variables detected for a 2D case.'
                write(*,*) '   Output not implemented.'
                return

            end if

        else  !! TRI-GLOBAL CASE!
            if (nvar == 5) then  ! 3D-Laminar flow
                write(fd,*) 'TITLE = "TAU"'
                write(fd,*) 'VARIABLES = "X" "Y" "Z" "Rho_r" "RhoU_r" "Rho_V" "RhoW_r" "RhoE_r"', &
                                '"Rho_i" "RhoU_i" "Rho_V_i" "RhoW_i" "RhoE_i " ', &
                                '"E_rho" "E_rhoU" "E_rhoV" "E_rhoW" "E_rhoE"'
                write(fd,*) 'ZONE T = "FP"'
                write(fd,*) 'I=',npoint,' J=1 K=1 ZONETYPE=Ordered'
                write(fd,*) 'DATAPACKING = POINT '
                do i=1,npoint
                    write(fd,'(18(2x,1pg0))') coord(i,1:ndim), &
                                                real(eigvec(i,1:nvar)), &
                                                aimag(eigvec(i,1:nvar)), &
                                                real(error(i,1:nvar))
                enddo

            else if (nvar == 6) then ! 3D-Turb 1eq
                write(fd,*) 'TITLE = "TAU"'
                write(fd,*) 'VARIABLES = "X" "Y" "Z" "Rho_r" "RhoU_r" "Rho_V" "RhoW_r"', &
                                '"RhoE_r" "Turb_r"', &
                                '"Rho_i" "RhoU_i" "Rho_V_i" "RhoW_i" "RhoE_i " "Turb_i"', &
                                '"E_rho" "E_rhoU" "E_rhoV" "E_rhoW" "E_rhoE" "E_turb"'
                write(fd,*) 'ZONE T = "FP"'
                write(fd,*) 'I=',npoint,' J=1 K=1 ZONETYPE=Ordered'
                write(fd,*) 'DATAPACKING = POINT '
                do i=1,npoint
                    write(fd,'(21(2x,1pg0))') coord(i,1:ndim), &
                                                real(eigvec(i,1:nvar)), &
                                                aimag(eigvec(i,1:nvar)), &
                                                real(error(i,1:nvar))
                enddo

            else if (nvar == 7) then ! 3D-Turb 2eq
                write(fd,*) 'TITLE = "TAU"'
                write(fd,*) 'VARIABLES = "X" "Y" "Z" "Rho_r" "RhoU_r" "Rho_V" "RhoW_r"', &
                                '"RhoE_r" "Turb1_r" "Turb2_r"', &
                                '"Rho_i" "RhoU_i" "Rho_V_i" "RhoW_i" "RhoE_i "', &
                                '"Turb1_i" "Turb2_i"', &
                                '"E_rho" "E_rhoU" "E_rhoV" "E_rhoW" "E_rhoE" "E_T1" "E_T2"'
                write(fd,*) 'ZONE T = "FP"'
                write(fd,*) 'I=',npoint,' J=1 K=1 ZONETYPE=Ordered'
                write(fd,*) 'DATAPACKING = POINT'
                do i=1,npoint
                    write(fd,'(24(2x,1pg0))') coord(i,1:ndim), &
                                                real(eigvec(i,1:nvar)), &
                                                aimag(eigvec(i,1:nvar)), &
                                                real(error(i,1:nvar))
                enddo

            else
                write(*,*) '   More than 7 variables detected for a 3D case.'
                write(*,*) '   Output not implemented.'
                return

            end if
        end if
        close(fd)
    end subroutine save2plt
!! ======================================================================== !!
end module eigvec2plt
