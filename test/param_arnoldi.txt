
#########################################

# ----- zTAUev Parameters File -----

#########################################

# ---------------- MAIN PARAMETERS -------------------

#     Working Directory (Absolute path)
WORKING_DIR = './'

#     Simulation Mach number
MACH = 0.845

#     TAU Matrix Scaling
TAU_SCALING = F

#     Load Jacobian Matrix from NetCDF file. If false, matrix is loaded from samg files from TAU (T/F)
READ_NETCDF = F

#     Save eigenvectors to PVAL format for TAU-like reading (T/F)
SAVE2PVAL = F

#     Compute eigenvectors (T/F)
COMPUTE_EIGENVECTORS = F

#
# ---------------- EGV SOLVER OPTIONS --------------------- #

#     Matrix factorization type (GMRES/LU)
FACTORIZATION = LU

#     Print linear solver statistics (T/F)
STATISTICS = F

#     Use ARPACK routines instead of simple ARNOLDI algorithm (T/F)
USE_ARPACK = F

#     Shift & Invert transformation parameter (Preconditioning)
SHIFT = (0.0,0.0)

#     Type of problem calculation (DIRECT/ADJOINT/BOTH)
#     If 'BOTH' is chosen, the Structural Sensitivity will be calculated
PROBLEM_TYPE = 'DIRECT'

#
# ----- ARNOLDI OPTIONS (IF USE_ARPACK=F) ------

#     ARNOLDI - Size of the Krylov subspace and number of calculated eigenvalues
KRYLOV_DIM = 6

#
# ----- ARPACK OPTIONS (IF USE_ARPACK=T) ------

#     EGV selection (SM/LM/SR/LR/SI/LI)
ARPACK_EGV_SELECTION = LM
#     ARPACK Number of requested Eigenvalues
NEV = 2
#     ARPACK Projected subspace dimension (recommended 3*NEV+1)
NCV = 6
#     ARPACK Linear Solver Tolerance
TOL = 1e-8

#
# ---------------- RESTART OPTIONS (CURRENTLY NOT WORKING) ------------------- #

#     Load Eigenvalues and Krylov matrix from file. If false, K matrix is calculated  (T/F)
LOAD_HK =  F
#     Save the Eigenvalues and Krylov matrix after the arnoldi iteration for further analysis (T/F)
SAVE_HK = F

#
# ---------------- ADVANCED FEATURES --------------------- #

#     Clean zero elements from Jacobian Matrix (recommended) (T/F)
CLEANZEROS = T

#     Extract a section of the Jacobian (Domain Reduction) (T/F)
REDUCE_JAC = F

#     Bounding Box for the Jacobian extraction (set real values)
ZMIN = -4.0
ZMAX = 4.0
XMIN = -2.0
XMAX = 10.0
#
# ----------------- INPUT FILES ----------------

#     Coordinates file (.coo)
COORDFILE = 'DATA/xzcoord.coo'

#     Info file (.frm)
INFOFILE = 'DATA/infofile.frm'

#     Volume matrix file (.vol)
VOLFILE = 'DATA/volumedata.vol'

#     Jacobian file (.amg). Not needed if LOAD_HK = T
MATFILE = 'DATA/jacobian_values.amg'

#     TAU Dual mesh
DUALGRID = 'DATA/dualgrid2D'

#     Eigenvalues & Krylov matrix input file. To compute eigenvectors from previous arnoldi iteration.
#     Krylov dim must be the same!!
HK_INFILE = 'DATA/hk.mat'

#
# ---------------- OUTPUT FILES ----------------

#    Eigenvalues file
EIGV_PATH = 'RESULTS/eigv_arnoldi'

#     Eigenvector prefix
EIGF_PATH = 'RESULTS/eigf'

#     Structural sensitivity file
SENS_PATH = 'RESULTS/sens'

#     Eigenvalues & Krylov matrix output file
HK_OUTFILE = 'DATA/hk.mat'
