program tauev
#include <petsc/finclude/petscsys.h>
    use precision_mod,   only: RP, isequal
    use paramlistClass,  only: paramlist
    use jac_matrix,      only: amatrix
    use egv_solver,      only: method
    use postprocess
    use petscsys
    implicit none
!! ---------------------------------------------------------------------!!
    type(paramlist)              :: params
    type(amatrix)                :: jacobian
    type(method)                 :: direct
    integer                      :: which_eigvec
    character(len=250)           :: arg, ifile, pout
    complex(kind=RP),allocatable :: dir_eigvec(:,:), dir_error(:,:)

    PetscErrorCode   ierr
    PetscMPIInt      rank, size
    PetscReal        T_START, T_END
!! ---------------------------------------------------------------------!!

    call PetscInitialize(PETSC_NULL_CHARACTER,ierr)
    if (ierr .ne. 0) then
      print*,'Unable to initialize PETSc'
      stop
    endif

    CALL MPI_Comm_rank(PETSC_COMM_WORLD,rank,ierr);CHKERRA(ierr)
    CALL MPI_Comm_size(PETSC_COMM_WORLD,size,ierr);CHKERRA(ierr)

    ! Print info
    if (rank .eq. 0) then
        write(*,*)
        write(*,*) " zTAUev"
        write(*,*) " Compiled : ", __DATE__,'  ', __TIME__
        write(*,*)
    end if

    call cpu_time(T_START)

    ! Gets param file path if available
    call get_command_argument(1, arg)
    if (len_trim(arg) == 0) then
        ifile = "param.txt"         !default param file
    else
        ifile = trim(arg)
    endif

    ! -- Get info from parameter file -----------------------
    write(pout,'(A)') ' Reading parameters from '//trim(ifile)//'\n\n'
    call PetscPrintf(PETSC_COMM_WORLD, trim(pout), ierr);CHKERRA(ierr)
    params = paramlist(ifile)
    call params%read_params()

    ! -- Contruct the Jacobian matrix for the analysis
    jacobian = amatrix(params)
    call jacobian%read_matrix(params)

    ! -- Construct the solver method
    direct  = method(jacobian, params)

    ! If LOAD_HK is true, V matrix is loaded from file instead of being
    ! calculated from jacobian matrix
    if (params%load_hk) then
        params%save_hk = .false.
        write(*,*) 'LOADING HESSEMBERG AND KRYLOV MATRICES FROM FILE:', &
                    trim(params%hkinfile)
        call direct%load_hk(jacobian, params)
        write(*,*) '   DONE'; write(*,*)
    else
        call direct%solve(params)
    end if

    call cpu_time(T_END)
    if (rank.eq.0) then
        write(*,'(A,ES16.3,A)') "TOTAL TIME REQUIRED FOR THE STABILITY ANALYSIS: ", T_END-T_START, ' sec.'
        write(*,*)
    end if

    if (rank.eq.0) then
        ! If SAVE_HK is true, EGVals and Krylov Matrices are stored
        if (params%save_hk) then
            write(*,*) ' SAVING EIGENVALUES AND KRYLOV MATRIX TO FILE: '//trim(params%hkoutfile)
            call direct%save_hk(jacobian, params)
            write(*,*) "   DONE"; write(*,*)
        endif
        ! --- COMPUTE AND SAVE EIGENVECTORS ------------
        if (params%comp_eigf) then
            write(*,*)
            write(*,*) "SAVING EIGENVECTORS: "
            DO
                write(*,*)
                print*,"   Print eigenvector number, (0 to terminate)"
                read (*,*) which_eigvec
                write(*,*)

                IF (which_eigvec <=0) exit

                write(*,*) ' Storing DIRECT mode...'
                call save_eigvec(which_eigvec, params, jacobian, direct, &
                                     dir_eigvec, dir_error)
            END DO
        end if
    end if
    call MPI_BARRIER(PETSC_COMM_WORLD, ierr)
    if (allocated(dir_eigvec)) deallocate(dir_eigvec)
    if (allocated(dir_error)) deallocate(dir_error)

    call jacobian%destroy()
    call PetscFinalize(ierr);CHKERRA(ierr)

end program
