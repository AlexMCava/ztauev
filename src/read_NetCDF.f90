MODULE read_NetCDF_module
    USE NetCDF

    INTEGER,        PARAMETER       ::  CP = 8
    INTEGER,        PARAMETER       ::  RP = 8

    COMPLEX(KIND = CP), PARAMETER   ::  r1 = (1.0_RP , 0.0_RP)
    COMPLEX(KIND = CP), PARAMETER   ::  i1 = (0.0_RP , 1.0_RP)

    TYPE SparseMatrix
        COMPLEX(KIND = CP), ALLOCATABLE     :: VAL(:)
        INTEGER,            ALLOCATABLE     :: row_ptr(:)
        INTEGER,            ALLOCATABLE     :: col_ind(:)
        INTEGER,            ALLOCATABLE     :: IRN(:)
        INTEGER,            ALLOCATABLE     :: JCN(:)

        INTEGER                             :: N
        INTEGER                             :: NEQ
        INTEGER                             :: NNZ
        INTEGER                             :: NDIM
        CONTAINS
            PROCEDURE :: read => loadSparseMatrix
            procedure calculate_irn
            procedure write_file
    END TYPE SparseMatrix

    INTERFACE SparseMatrix
        MODULE PROCEDURE newSparseMatrix
    END INTERFACE SparseMatrix

    PRIVATE
    PUBLIC SparseMatrix

    CONTAINS
!-------------------------------------------------------------------------------
        FUNCTION newSparseMatrix() RESULT(S)
            IMPLICIT NONE
            TYPE(SparseMatrix) :: S

            S % N    = 0
            S % NNZ  = 0
            S % NEQ  = 0
            S % NDIM = 0

            IF (ALLOCATED(S % VAL))     DEALLOCATE( S % VAL )
            IF (ALLOCATED(S % row_ptr)) DEALLOCATE( S % row_ptr )
            IF (ALLOCATED(S % col_ind)) DEALLOCATE( S % col_ind )
            IF (ALLOCATED(S % IRN)) DEALLOCATE( S % IRN )
            IF (ALLOCATED(S % JCN)) DEALLOCATE( S % JCN )

        END FUNCTION newSparseMatrix
!-------------------------------------------------------------------------------
        SUBROUTINE loadSparseMatrix(this , Name)
            IMPLICIT NONE
            CLASS(SparseMatrix)             :: this
            CHARACTER(LEN = *)              :: Name
            INTEGER                         :: fID
            INTEGER                         :: nDim , nVar
            INTEGER                         :: nFound
            INTEGER                         :: dim , var
            CHARACTER(LEN = 100)            :: str
            REAL(KIND = RP), ALLOCATABLE    :: aux(:)
            INTEGER                         :: i

!           Open file
            CALL check( NF90_OPEN (TRIM(Name) , NF90_NOWRITE , fID  ) )
!           Read N, NEC, and NNZ
            CALL check ( NF90_INQUIRE( fID , nDim, nVar ) )

!           Loop to gather dimensions
            nfound = 0
            !write(*,*) "Check", nDim, nVar
            DO dim = 1 , nDim
                CALL check( NF90_INQUIRE_DIMENSION( fID , dim ,str ) )

                IF (TRIM(str) == "nvars") THEN
                    !write(*,*) "Reading numvar"
                    CALL check( NF90_INQUIRE_DIMENSION(fID, dim, str, this%N ))
                    this%N = this%N - 1   ! Because N+1 is stored in the file
                    nFound = nFound + 1

                ELSEIF (TRIM(str) == "nnz") THEN
                    CALL check( NF90_INQUIRE_DIMENSION(fID, dim, str, this%NNZ))
                    nFound = nFound + 1

                ELSEIF (TRIM(str) == "neq") THEN
                    CALL check( NF90_INQUIRE_DIMENSION(fID, dim, str, this%NEQ))
                    nFound = nFound + 1

                ELSEIF (TRIM(str) == "ndim") THEN
                    CALL check( NF90_INQUIRE_DIMENSION(fID, dim, str, this%NDIM))
                    nFound = nFound + 1

                END IF
            END DO

            ! Allocate variables
            ALLOCATE( this % row_ptr ( this % N + 1 ) )
            ALLOCATE( this % col_ind ( this % NNZ   ) )
            ALLOCATE( this % val     ( this % NNZ   ) )
            ALLOCATE( aux            ( this % NNZ   ) )

            this % val = cmplx((0.0d0, 0.0d0), KIND=RP)

            ! Read variables
            DO var = 1 , nVar
                CALL check( NF90_INQUIRE_VARIABLE( fID , var , str ))

                IF (TRIM(str) == "row_ptr") THEN
                    CALL check( NF90_GET_VAR( fID , var , this % row_ptr ) )
                ELSE IF (TRIM(str) == "col_ind") THEN
                    CALL check( NF90_GET_VAR( fID , var , this % col_ind ) )
                ELSE IF (TRIM(str) == "data") THEN
                    CALL check( NF90_GET_VAR( fID , var , aux ) )
                    this % val = this % val + aux*r1
                ELSE IF (TRIM(str) == "val_real") THEN
                    CALL check( NF90_GET_VAR( fID , var , aux ) )
                    this % val = this % val + aux*r1
                ELSE IF (TRIM(str) == "val_comp") THEN
                    CALL check( NF90_GET_VAR( fID , var , aux ) )
                    this % val = this % val + aux*i1
                END IF

            END DO

            ! Close file
            CALL check( NF90_CLOSE( fID ) )

            ! Transformation
            ALLOCATE( this % IRN( this % NNZ) )
            ALLOCATE( this % JCN( this % NNZ) )

            this % JCN = this % col_ind

            DO i = 1 , this % N
                this % IRN(this % row_ptr(i) : this % row_ptr(i+1)-1) = i
            END DO

            ! DEALLOCATE( this % row_ptr)
            ! DEALLOCATE( this % col_ind )


        END SUBROUTINE loadSparseMatrix
!-------------------------------------------------------------------------------
        SUBROUTINE calculate_irn(this)
        ! In case external calculation of CRS indexes is needed
            CLASS(SparseMatrix) :: this
            IF (ALLOCATED(this % IRN)) DEALLOCATE( this % IRN )
            IF (ALLOCATED(this % JCN)) DEALLOCATE( this % JCN )
            ALLOCATE( this % IRN( this % NNZ) )
            ALLOCATE( this % JCN( this % NNZ) )

            this % JCN = this % col_ind

            DO i = 1, this%N
                this%IRN( this%row_ptr(i) : this%row_ptr(i+1)-1 ) = i
            END DO

        END SUBROUTINE calculate_irn
!-------------------------------------------------------------------------------
        ! SUBROUTINE calculate_rowptr(this)
        !     CLASS(SparseMatrix) :: this
        !     integer  :: i, counter
        !
        !     allocate( this % row_ptr( this%nvar) )
        !     allocate( this % col_ind( this%nnz) )
        !
        !     this%col_ind = this%JCN
        !     this%row_ptr(1) = 1
        !     counter = 1
        !     do i = 1, this%nnz
        !         if (this%IRN(i) /= counter) then
        !             counter = counter + 1
        !             this%row_ptr(counter) = this%IRN(i)
        ! END SUBROUTINE calculate_rowptr
!-------------------------------------------------------------------------------
        SUBROUTINE write_file(this, outfile)
            CLASS(SparseMatrix) :: this
            character(len=*)    :: outfile
            integer :: ncid, nvars_id, nnz_id, neq_id, ndim_id, &
                        rptr_id, cind_id, data_id

            call check( nf90_create(trim(outfile), nf90_clobber, ncid) )

            call check( nf90_def_dim(ncid, 'nvars', this%N, nvars_id ) )
            call check( nf90_def_dim(ncid, 'nnz', this%nnz, nnz_id ) )
            call check( nf90_def_dim(ncid, 'neq', this%neq, neq_id ) )
            call check( nf90_def_dim(ncid, 'ndim', this%ndim, ndim_id ) )

            call check( nf90_def_var(ncid, 'row_ptr', NF90_INT, nvars_id, rptr_id) )
            call check( nf90_def_var(ncid, 'col_ind', NF90_INT, nnz_id, cind_id) )
            call check( nf90_def_var(ncid, 'data', NF90_DOUBLE, nnz_id, data_id) )

            call check( nf90_put_var(ncid, rptr_id, this%row_ptr) )
            call check( nf90_put_var(ncid, cind_id, this%col_ind) )
            call check( nf90_put_var(ncid, data_id, real(this%val)) )

            call check( nf90_close(ncid) )
        END SUBROUTINE write_file
!-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------
    !   Check subroutine: In order to understand NetCDF
    !   potential errors
    !-------------------------------------------------------------------------
        SUBROUTINE check (status)
            INTEGER, INTENT(IN)     :: status

            IF (status /= NF90_NOERR)       THEN
                PRINT*, TRIM(NF90_STRERROR(status))
                STOP "Stopped"
            END IF
        END SUBROUTINE check

END MODULE read_NetCDF_module
