module jacobian_tools
#include <petsc/finclude/petscsys.h>
    use read_NetCDF_module
    use precision_mod,   only: RP
    use math_tools
    use petscsys
    implicit none

    PetscErrorCode   ierr
    PetscMPIInt      rank, size

contains
!-------------------------------------------------------------------------------
    subroutine cleanzeros(A)
        type (SparseMatrix)          :: A
        type (SparseMatrix)          :: C
        integer                      :: counter_data, i
        REAL(KIND=RP), ALLOCATABLE   :: A_data(:), A_res(:)
        character(len=250)           :: pout


        ! COUNT THE NUMBER OF "REAL"-NON-ZERO ELEMENTS UNDER A SET CRITERIA
        write(pout,*) 'ELIMINATING ZEROS FROM SPARSE MATRIX...\n'
        call PetscPrintf(PETSC_COMM_WORLD, pout, ierr)
        counter_data = 0
        allocate(A_data(A%nnz))
        A_data = real(A%val)
        DO i=1, A%nnz
            IF(abs(A_data(i)).gt.1d-9) then
                counter_data = counter_data+1
            END IF
        END DO

        ! ALLOCATE NEW CRS VECTORS WITH CORRECTED DIMENSIONS
        C = SparseMatrix()
        C%nnz = counter_data
        C%neq = A%neq
        C%ndim = A%ndim
        C%N = A%N
        ALLOCATE( C%IRN ( C%nnz ) )
        ALLOCATE( C%JCN ( C%nnz ) )
        ALLOCATE( C%val ( C%nnz ) )

        ! LOOP OVER OLD ARRAYS, STORING NON-ZERO ELEMENTS IN NEW ARRAYS
        counter_data = 0
        DO i=1, A%nnz
            IF(abs(A_data(i)).gt.1d-9) then
                counter_data = counter_data+1
                C%IRN(counter_data) = A%IRN(i)
                C%JCN(counter_data) = A%JCN(i)
                C%val(counter_data) = A%val(i)
            END IF
        END DO

        ! print*, 'Before = ', A%nnz, ' **** After = ', C%nnz
        write(pout,*) 'Before = ', A%nnz, ' **** After = ', C%nnz, '\n'
        call PetscPrintf(PETSC_COMM_WORLD, trim(pout),ierr)
        ! PRINT*,

        DEALLOCATE(A%IRN, A%JCN, A%VAL, A%row_ptr, A%col_ind)
        A%nnz = C%nnz

        ! ALLOCATING NEW ARRAYS WITH REDUCED DIMENSION
        ALLOCATE( A%IRN (A%nnz) )
        ALLOCATE( A %JCN (A%nnz) )
        ALLOCATE( A%val (A%nnz) )
        ALLOCATE( A%row_ptr (A%N+1) )
        ALLOCATE( A%col_ind (A%nnz) )
        A%IRN = C%IRN
        A%JCN = C%JCN
        A%val = C%val
        DEALLOCATE(C%IRN, C%JCN, C%VAL, A_data)

        ! RECOVERING CSR DATA STRUCTURE
        allocate(A_data(A%nnz), A_res(A%nnz))
        A_data = real(A%val)
        call coocsr(A%N,A%NNZ,A_data,A%IRN,A%JCN,A_res,A%col_ind,A%row_ptr)
        deallocate(A_data, A_res)

    end subroutine cleanzeros
!-------------------------------------------------------------------------------
    subroutine permutation(A, coord, vol, lid, zmin, zmax, xmin, xmax)
        type(SparseMatrix)          :: A
        real(kind=RP), intent(in)   :: coord(1:,1:), vol(1:)
        integer      , intent(in)   :: lid(1:)
        real(kind=RP), intent(in)   :: zmin, zmax, xmin, xmax
        integer                     :: M, counter_reduced

        integer                     :: i, ii, jj, ierr, numnod
        real(kind=RP), allocatable  :: xcoord(:), zcoord(:), A_data(:), &
                                    A_res(:), C_VAL(:), volumes(:), localid(:)
        real(kind=RP), allocatable  :: Perm(:), PermO(:), PermOT(:)

        ! I-J coord of each permutation matrix element
        integer, allocatable        :: PermI(:), PermJ(:), &
                                       PermIO(:), PermJO(:),&
                                       PermIOT(:), PermJOT(:), &
                                       iw(:), C_IRN(:), C_JCN(:)
        character(len=250)          :: outfile, pout
        type(SparseMatrix)          :: C

        numnod = A%N

        ! PERMUTATION MATRIX
        ALLOCATE( Perm(numnod), PermI(numnod), PermJ(numnod))

        ! NON-ZERO ELEMENTS HAVE A VALUE OF 1 ON PERMUTATION MATRIX
        Perm = 1.0d0
        ! INITIALIZE COORDINATES
        PermI = 0
        PermJ = 0

        ii = 0
        jj = 0

        DO i=1, numnod
            IF ( (coord(i,2) .gt. zmin) .and. &
                 (coord(i,2) .lt. zmax) .and. &
                 (coord(i,1) .gt. xmin) .and. &
                 (coord(i,1) .lt. xmax) ) then
                ii = ii + 1
                PermI(i) = ii
                PermJ(i) = i
            ELSE
                PermI(i) = numnod - jj
                PermJ(i) = i
                jj = jj + 1
            END IF
        END DO

        M = ii
        write(pout,*) 'New-dim = ', ii, ' Current-dim = ', A%N, '\n'
        call PetscPrintf(PETSC_COMM_WORLD, trim(pout),ierr)

        ALLOCATE( PermIO(numnod+1), PermIOT(numnod + 1) )
        ALLOCATE( PermJO(numnod), PermJOT(numnod) )
        ALLOCATE( PermO(numnod), PermOT(numnod) )

        write(pout,*) 'COO matrix to CSR...\n'
        call PetscPrintf(PETSC_COMM_WORLD, trim(pout),ierr)
        call coocsr(numnod, numnod, Perm, PermI, PermJ, PermO, PermJO, PermIO)
        call coocsr(numnod, numnod, Perm, PermJ, PermI, PermOT, PermJOT, PermIOT)

        DEALLOCATE(Perm, PermI, PermJ)

        ALLOCATE(xcoord(numnod), zcoord(numnod))
        write(pout,*) 'REORDERING COORDINATES...\n'
        call PetscPrintf(PETSC_COMM_WORLD, trim(pout),ierr)
        call amux(numnod, coord(1:numnod,1), xcoord, PermO, PermJO, PermIO)
        call amux(numnod, coord(1:numnod,2), zcoord, PermO, PermJO, PermIO)

        ALLOCATE(volumes(numnod))
        write(pout,*) 'REORDERING VOLUMES...\n'
        call PetscPrintf(PETSC_COMM_WORLD, trim(pout),ierr)
        call amux(numnod, vol(1:numnod), volumes, PermO, PermJO, PermIO)

        ALLOCATE(localid(numnod))
        write(pout,*) 'REORDERING LOCALID...\n'
        !! LOCAL_ID array converted to double from integer to avoid writing
        !! an extra routine for sparse matrix multiplication
        call PetscPrintf(PETSC_COMM_WORLD, trim(pout),ierr)
        call amux(numnod, dble(lid(1:numnod)), localid, PermO, PermJO, PermIO)

        ! Write coordinates of reduced domain
        write(pout,*) 'WRITING NEW COORDINATES TO FILE...\n'
        call PetscPrintf(PETSC_COMM_WORLD, trim(pout),ierr)
        open(30, file='newcoor.dat', action='WRITE')
        write(30,*) ii, 2
        DO i = 1, ii !A%numnod
            write(30,*) xcoord(i), zcoord(i)
        END DO
        close(30)

        ! Write volumes of reduced domain
        write(pout,*) 'WRITING NEW CELL VOLUMES TO FILE...\n'
        call PetscPrintf(PETSC_COMM_WORLD, trim(pout),ierr)
        open(30, file='newvol.dat', action='WRITE')
        DO i = 1, ii, A%neq !A%numnod
            write(30,*) volumes(i)
        END DO
        close(30)

        ! Write volumes of reduced domain
        write(pout,*) 'WRITING NEW LOCAL_ID VALUES TO FILE...\n'
        call PetscPrintf(PETSC_COMM_WORLD, trim(pout),ierr)
        open(30, file='newlocalid.dat', action='WRITE')
        DO i = 1, ii, A%neq !A%numnod
            write(30,*) int(localid(i))
        END DO
        close(30)

        print*, 'WRITING PERMUTED COORDINATES TO FILE...'
        open(30, file='permuted_coor.dat', action='WRITE')
        DO i = 1, numnod
            write(30,*) xcoord(i), zcoord(i), volumes(i), int(localid(i))
        END DO
        close(30)
        DEALLOCATE(xcoord, zcoord, volumes, localid)

        write(pout,*) 'STARTING PERMUTATION OF JACOBIAN MATRIX...\n'
        call PetscPrintf(PETSC_COMM_WORLD, trim(pout),ierr)

        ALLOCATE(C_IRN(A%N+1), C_JCN(A%nnz), C_VAL(A%nnz))
        ALLOCATE(iw(A%N))
        ALLOCATE(A_data(A%nnz))

        A_data = real(A%val)

        call amub(A%N, A%N, 1, A_data, A%col_ind, A%row_ptr, &
            PermOT, PermJOT, PermIOT, C_VAL, C_JCN, C_IRN, A%nnz, iw, ierr)
        ! print*, 'ierr', ierr

        A_data = 0.0d0
        A%row_ptr = 0
        A%col_ind = 0

        call amub(A%N, A%N, 1, PermO, PermJO, PermIO, C_VAL, &
            C_JCN, C_IRN, A_data, A%col_ind, A%row_ptr, A%nnz, iw, ierr)
        print*, 'ierr', ierr

        do i=1,A%nnz
            A%val(i) = A_data(i) * (1.0_RP, 0.0_RP)
        end do

        call A%calculate_irn()
        ! A%JCN = A%col_ind

        DEALLOCATE(C_IRN, C_JCN, C_VAL, A_data, iw)
        DEALLOCATE(PermO, PermJO, PermIO, PermOT, PermJOT, PermIOT)

        ! Dump data into new files
        ! open(newunit=30, file="Adata.dat", action='WRITE')
        ! open(newunit=31, file="Irows.dat",action='WRITE')
        ! open(newunit=32, file="Jcolumns.dat", action='WRITE')

        call PetscPrintf(PETSC_COMM_WORLD, '  DONE\n',ierr)
        outfile = 'permuted_jac.dat'
        ! call A%write_file(outfile)

        write(pout,*) 'NNZ Pre-cut = ', A%NNZ, '\n'
        call PetscPrintf(PETSC_COMM_WORLD, trim(pout),ierr)
        !!! 'CUTTING' JACOBIAN MATRIX
        !M = newdim   ! "M" IS THE DIMENSION OF THE SUBMATRIX OF INTEREST
        write(pout,*) '    "CUTTING" JACOBIAN MATRIX \n'
        call PetscPrintf(PETSC_COMM_WORLD, trim(pout),ierr)
        C = SparseMatrix()
        C%nnz = A%NNZ
        C%neq = A%neq
        C%ndim = A%ndim
        C%N = A%N
        ALLOCATE( C%IRN ( C%nnz ) )
        ALLOCATE( C%JCN ( C%nnz ) )
        ALLOCATE( C%val ( C%nnz ) )
        C%IRN = A%IRN
        C%JCN = A%JCN
        C%val = A%VAL

        counter_reduced = 0
        DO i=1, C%nnz
            IF(C%IRN(i).le.M .and. C%JCN(i).le.M) then
                counter_reduced = counter_reduced + 1
            END IF
        END DO

        DEALLOCATE(A%IRN, A%JCN, A%VAL, A%row_ptr, A%col_ind)
        A%nnz = counter_reduced
        A%N = M

        ! ALLOCATING NEW ARRAYS WITH REDUCED DIMENSION
        ALLOCATE( A%IRN (A%nnz) )
        ALLOCATE( A%JCN (A%nnz) )
        ALLOCATE( A%val (A%nnz) )
        ALLOCATE( A%row_ptr (A%N+1) )
        ALLOCATE( A%col_ind (A%nnz) )

        ! PASSING DATA...
        counter_reduced = 0
        DO i=1,C%nnz
            IF(C%IRN(i).le.M .and. C%JCN(i).le.M) then
                counter_reduced = counter_reduced + 1
                A%IRN(counter_reduced) = C%IRN(i)
                A%JCN(counter_reduced) = C%JCN(i)
                A%val(counter_reduced) = C%val(i)
            END IF
        END DO

        DEALLOCATE(C%IRN, C%JCN, C%VAL)
        write(pout,*) 'NNZ Post-cut = ', A%NNZ, '\n'
        call PetscPrintf(PETSC_COMM_WORLD, trim(pout),ierr)

        ! RECOVERING CSR DATA STRUCTURE
        allocate(A_data(A%nnz), A_res(A%nnz))
        A_data = real(A%val)
        call coocsr(A%N,A%NNZ,A_data,A%IRN,A%JCN,A_res,A%col_ind,A%row_ptr)
        ! call A%write_file('reduced_jac.dat')
        call PetscPrintf(PETSC_COMM_WORLD, '   DONE \n',ierr)

        !A%val = A_res * (1.0d0, 0.0)
        deallocate(A_data, A_res)

    end subroutine permutation
!-------------------------------------------------------------------------------
subroutine permutation3D(A, coord, vol, lid, zmin, zmax, ymin, ymax, xmin, xmax)
    type(SparseMatrix)          :: A
    real(kind=RP), intent(in)   :: coord(1:,1:), vol(1:)
    integer      , intent(in)   :: lid(1:)
    real(kind=RP), intent(in)   :: zmin, zmax, ymin, ymax, xmin, xmax
    integer                     :: M, counter_reduced

    integer                     :: i, ii, jj, ierr, numnod
    real(kind=RP), allocatable  :: xcoord(:), ycoord(:), zcoord(:), A_data(:), &
                                A_res(:), C_VAL(:), volumes(:), localid(:)
    real(kind=RP), allocatable  :: Perm(:), PermO(:), PermOT(:)

    ! I-J coord of each permutation matrix element
    integer, allocatable        :: PermI(:), PermJ(:), &
                                   PermIO(:), PermJO(:),&
                                   PermIOT(:), PermJOT(:), &
                                   iw(:), C_IRN(:), C_JCN(:)
    character(len=250)          :: outfile, pout
    type(SparseMatrix)          :: C

    numnod = A%N

    ! PERMUTATION MATRIX
    ALLOCATE( Perm(numnod), PermI(numnod), PermJ(numnod))

    ! NON-ZERO ELEMENTS HAVE A VALUE OF 1 ON PERMUTATION MATRIX
    Perm = 1.0d0
    ! INITIALIZE COORDINATES
    PermI = 0
    PermJ = 0

    ii = 0
    jj = 0

    DO i=1, numnod
        IF ( (coord(i,3) .gt. zmin) .and. &
             (coord(i,3) .lt. zmax) .and. &
             (coord(i,2) .lt. ymax) .and. &
             (coord(i,2) .gt. ymin) .and. &
             (coord(i,1) .gt. xmin) .and. &
             (coord(i,1) .lt. xmax) ) then
            ii = ii + 1
            PermI(i) = ii
            PermJ(i) = i
        ELSE
            PermI(i) = numnod - jj
            PermJ(i) = i
            jj = jj + 1
        END IF
    END DO

    M = ii
    write(pout,*) 'New-dim = ', ii, ' Current-dim = ', A%N, '\n'
    call PetscPrintf(PETSC_COMM_WORLD, trim(pout),ierr)

    ALLOCATE( PermIO(numnod+1), PermIOT(numnod + 1) )
    ALLOCATE( PermJO(numnod), PermJOT(numnod) )
    ALLOCATE( PermO(numnod), PermOT(numnod) )

    write(pout,*) 'COO matrix to CSR...\n'
    call PetscPrintf(PETSC_COMM_WORLD, trim(pout),ierr)
    call coocsr(numnod, numnod, Perm, PermI, PermJ, PermO, PermJO, PermIO)
    call coocsr(numnod, numnod, Perm, PermJ, PermI, PermOT, PermJOT, PermIOT)

    DEALLOCATE(Perm, PermI, PermJ)

    ALLOCATE(xcoord(numnod), ycoord(numnod), zcoord(numnod))
    write(pout,*) 'REORDERING COORDINATES...\n'
    call PetscPrintf(PETSC_COMM_WORLD, trim(pout),ierr)
    call amux(numnod, coord(1:numnod,1), xcoord, PermO, PermJO, PermIO)
    call amux(numnod, coord(1:numnod,2), ycoord, PermO, PermJO, PermIO)
    call amux(numnod, coord(1:numnod,3), zcoord, PermO, PermJO, PermIO)

    ALLOCATE(volumes(numnod))
    write(pout,*) 'REORDERING VOLUMES...\n'
    call PetscPrintf(PETSC_COMM_WORLD, trim(pout),ierr)
    call amux(numnod, vol(1:numnod), volumes, PermO, PermJO, PermIO)

    ALLOCATE(localid(numnod))
    write(pout,*) 'REORDERING LOCALID...\n'
    !! LOCAL_ID array converted to double from integer to avoid writing
    !! an extra routine for sparse matrix multiplication
    call PetscPrintf(PETSC_COMM_WORLD, trim(pout),ierr)
    call amux(numnod, dble(lid(1:numnod)), localid, PermO, PermJO, PermIO)

    ! Write coordinates of reduced domain
    write(pout,*) 'WRITING NEW COORDINATES TO FILE...\n'
    call PetscPrintf(PETSC_COMM_WORLD, trim(pout),ierr)
    open(30, file='newcoor.dat', action='WRITE')
    write(30,*) ii, 3
    DO i = 1, ii !A%numnod
        write(30,*) xcoord(i), ycoord(i), zcoord(i)
    END DO
    close(30)

    ! Write volumes of reduced domain
    write(pout,*) 'WRITING NEW CELL VOLUMES TO FILE...\n'
    call PetscPrintf(PETSC_COMM_WORLD, trim(pout),ierr)
    open(30, file='newvol.dat', action='WRITE')
    DO i = 1, ii, A%neq !A%numnod
        write(30,*) volumes(i)
    END DO
    close(30)

    ! Write volumes of reduced domain
    write(pout,*) 'WRITING NEW LOCAL_ID VALUES TO FILE...\n'
    call PetscPrintf(PETSC_COMM_WORLD, trim(pout),ierr)
    open(30, file='newlocalid.dat', action='WRITE')
    DO i = 1, ii, A%neq !A%numnod
        write(30,*) int(localid(i))
    END DO
    close(30)

    print*, 'WRITING PERMUTED COORDINATES TO FILE...'
    open(30, file='permuted_coor.dat', action='WRITE')
    DO i = 1, numnod
        write(30,*) xcoord(i), ycoord(i), zcoord(i), volumes(i), int(localid(i))
    END DO
    close(30)
    DEALLOCATE(xcoord, ycoord, zcoord, volumes, localid)

    write(pout,*) 'STARTING PERMUTATION OF JACOBIAN MATRIX...\n'
    call PetscPrintf(PETSC_COMM_WORLD, trim(pout),ierr)

    ALLOCATE(C_IRN(A%N+1), C_JCN(A%nnz), C_VAL(A%nnz))
    ALLOCATE(iw(A%N))
    ALLOCATE(A_data(A%nnz))

    A_data = real(A%val)

    call amub(A%N, A%N, 1, A_data, A%col_ind, A%row_ptr, &
        PermOT, PermJOT, PermIOT, C_VAL, C_JCN, C_IRN, A%nnz, iw, ierr)
    ! print*, 'ierr', ierr

    A_data = 0.0d0
    A%row_ptr = 0
    A%col_ind = 0

    call amub(A%N, A%N, 1, PermO, PermJO, PermIO, C_VAL, &
        C_JCN, C_IRN, A_data, A%col_ind, A%row_ptr, A%nnz, iw, ierr)
    print*, 'ierr', ierr

    do i=1,A%nnz
        A%val(i) = A_data(i) * (1.0_RP, 0.0_RP)
    end do

    call A%calculate_irn()
    ! A%JCN = A%col_ind

    DEALLOCATE(C_IRN, C_JCN, C_VAL, A_data, iw)
    DEALLOCATE(PermO, PermJO, PermIO, PermOT, PermJOT, PermIOT)

    ! Dump data into new files
    ! open(newunit=30, file="Adata.dat", action='WRITE')
    ! open(newunit=31, file="Irows.dat",action='WRITE')
    ! open(newunit=32, file="Jcolumns.dat", action='WRITE')

    call PetscPrintf(PETSC_COMM_WORLD, '  DONE\n',ierr)
    outfile = 'permuted_jac.dat'
    ! call A%write_file(outfile)

    write(pout,*) 'NNZ Pre-cut = ', A%NNZ, '\n'
    call PetscPrintf(PETSC_COMM_WORLD, trim(pout),ierr)
    !!! 'CUTTING' JACOBIAN MATRIX
    !M = newdim   ! "M" IS THE DIMENSION OF THE SUBMATRIX OF INTEREST
    write(pout,*) '    "CUTTING" JACOBIAN MATRIX \n'
    call PetscPrintf(PETSC_COMM_WORLD, trim(pout),ierr)
    C = SparseMatrix()
    C%nnz = A%NNZ
    C%neq = A%neq
    C%ndim = A%ndim
    C%N = A%N
    ALLOCATE( C%IRN ( C%nnz ) )
    ALLOCATE( C%JCN ( C%nnz ) )
    ALLOCATE( C%val ( C%nnz ) )
    C%IRN = A%IRN
    C%JCN = A%JCN
    C%val = A%VAL

    counter_reduced = 0
    DO i=1, C%nnz
        IF(C%IRN(i).le.M .and. C%JCN(i).le.M) then
            counter_reduced = counter_reduced + 1
        END IF
    END DO

    DEALLOCATE(A%IRN, A%JCN, A%VAL, A%row_ptr, A%col_ind)
    A%nnz = counter_reduced
    A%N = M

    ! ALLOCATING NEW ARRAYS WITH REDUCED DIMENSION
    ALLOCATE( A%IRN (A%nnz) )
    ALLOCATE( A%JCN (A%nnz) )
    ALLOCATE( A%val (A%nnz) )
    ALLOCATE( A%row_ptr (A%N+1) )
    ALLOCATE( A%col_ind (A%nnz) )

    ! PASSING DATA...
    counter_reduced = 0
    DO i=1,C%nnz
        IF(C%IRN(i).le.M .and. C%JCN(i).le.M) then
            counter_reduced = counter_reduced + 1
            A%IRN(counter_reduced) = C%IRN(i)
            A%JCN(counter_reduced) = C%JCN(i)
            A%val(counter_reduced) = C%val(i)
        END IF
    END DO

    DEALLOCATE(C%IRN, C%JCN, C%VAL)
    write(pout,*) 'NNZ Post-cut = ', A%NNZ, '\n'
    call PetscPrintf(PETSC_COMM_WORLD, trim(pout),ierr)

    ! RECOVERING CSR DATA STRUCTURE
    allocate(A_data(A%nnz), A_res(A%nnz))
    A_data = real(A%val)
    call coocsr(A%N,A%NNZ,A_data,A%IRN,A%JCN,A_res,A%col_ind,A%row_ptr)
    ! call A%write_file('reduced_jac.dat')
    call PetscPrintf(PETSC_COMM_WORLD, '   DONE \n',ierr)

    !A%val = A_res * (1.0d0, 0.0)
    deallocate(A_data, A_res)

end subroutine permutation3D
!-------------------------------------------------------------------------------
end module jacobian_tools
