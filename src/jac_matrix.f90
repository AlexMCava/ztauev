module jac_matrix
#include <petsc/finclude/petscsys.h>
    use netcdf
    use precision_mod,   only: RP
    use paramlistClass
    use jacobian_tools,  only: cleanzeros, permutation, permutation3D, csrcsc
    use read_NetCDF_module
    use petscsys

    implicit none
    private
    public amatrix

    PetscErrorCode   ierr
    PetscMPIInt      rank, size

    type amatrix
        integer, allocatable          :: row_ptr(:)
        integer, allocatable          :: col_ind(:)
        integer, allocatable          :: row_ptrT(:)
        integer, allocatable          :: col_indT(:)
        integer, allocatable          :: IRN(:)
        integer, allocatable          :: JCN(:)
        COMPLEX(kind=RP), allocatable :: data(:)
        COMPLEX(kind=RP), allocatable :: dataT(:)
        real(kind=RP), allocatable    :: coord(:,:)
        real(kind=RP), allocatable    :: vol(:)
        integer, allocatable          :: localid(:)
        integer                       :: ndim       ! Number of dimensions
        integer                       :: neq        ! Number of variables. (DOF per point)
        integer                       :: nvar       ! Number of degrees of freedom (Number of nodes * NEQ)
        integer                       :: nnz        ! Number of non-zero Jacobian elements
        real(kind=RP), private        :: afactor    ! Mach correction for TAU values

    CONTAINS
        procedure, private :: read_coordinates
        procedure, private :: read_volumes
        procedure          :: read_matrix
        procedure          :: destroy
    end type amatrix

    TYPE (SparseMatrix) S

    interface amatrix
        procedure new_matrix
    end interface

contains
!! ======================================================================== !!
    function new_matrix(params) result(new)
        !! Constructor. Reads main parameters from PARA file.
        type(paramlist), intent(in) :: params
        type(amatrix)               :: new

        new%afactor = 1d0 / (params%mach*sqrt(1.4d0))

        call MPI_Comm_rank(PETSC_COMM_WORLD, rank, ierr);CHKERRA(ierr)
        CALL MPI_Comm_size(PETSC_COMM_WORLD, size, ierr);CHKERRA(ierr)

        if (rank.eq.0) then
            write(*,*)
            write(*,'(A,F8.4)') 'PROBLEM MACH NUMBER = ', params%mach
            write(*,'(A,F8.4)') 'PROBLEM REYNOLDS_LENGTH = ', params%rlength
            write(*,*)
        end if

    end function new_matrix
!! ======================================================================== !!
    subroutine read_coordinates(this, params)
        ! Read coordinates from COO file
        class(amatrix),  intent(inout)  :: this
        class(paramlist), intent(in)    :: params
        character(len=250)              :: pout
        integer                         :: ndof, cooh, i

        ! ------ READ COORDINATES ------------------------------
        write(pout,'(a)') "READING DIMENSIONS FROM COORD FILE: " // &
                            trim(params%coordfile) // '\n'
        call PetscPrintf(PETSC_COMM_WORLD, trim(pout), ierr)

        ! Open coord file
        open(newunit=cooh, file=trim(params%coordfile), action='READ', status='OLD')
        ! reads nallpoints*ncons  and number of dimensions only ndim used
        read(cooh,*) ndof, this%ndim

        !! Simple warning in case NDOF was not correctly defined in the COO file
        this%nvar = count_coordinates(cooh)
        IF (this%nvar /= ndof) then
            if (rank .eq. 0) then
                print*, 'WARNING! Number of nodes was corrected after reading COO file.'
                print*, 'Counted = ', this%nvar, ' || On file = ', ndof
                write(*,*)
            end if
        END IF

        rewind(cooh)

        !! Allocates coordinates vector and reads it
        if (allocated(this%coord)) deallocate(this%coord)
        allocate(this%coord(this%nvar,this%ndim))
        read(cooh,*) ! read header
        do i = 1, this%nvar
            read(cooh,*) this%coord(i, 1:this%ndim) ! read coordinates
        end do
        do i = 1, this%nvar ! Correction with Reynolds Length
            this%coord(i, 1:this%ndim) = this%coord(i, 1:this%ndim) * params%rlength
        end do

        close(cooh)
    end subroutine read_coordinates
!! ======================================================================== !!
    subroutine read_volumes(this, params)
        ! Read volumes from VOL file
        class(amatrix),  intent(inout)  :: this
        class(paramlist), intent(in)    :: params
        character(len=250)              :: pout
        integer                         :: vfile, i, eq

        if (params%generalized) then
            ! ------ READ VOLUMES ------------------------------
            write(pout,'(a)') "READING DIMENSIONS FROM VOL FILE: " // &
                                trim(params%volfile) // '\n'
            call PetscPrintf(PETSC_COMM_WORLD, trim(pout), ierr)

            ! Open vol file
            open(newunit=vfile, file=trim(params%volfile), action='READ', status='OLD')

            if (allocated(this%vol)) deallocate(this%vol)
            allocate(this%vol(this%nvar))

            do i = 1, this%nvar, this%neq
                read(vfile,*)  this%vol(i) ! read volumes
                do eq = 1, this%neq-1
                    this%vol(i+eq) = this%vol(i) ! insert extra values for volumes
                end do
            end do
        else
            write(*,*) "Not generalized eigenvalue problem chosen."
            write(*,*) "Generating volume matrix as identity matrix"
            write(*,*)
            this%vol = 1d0
        end if

        close(vfile)
    end subroutine read_volumes
!! ======================================================================== !!
    subroutine read_localid(this, params, cdfbool)
        ! Read volumes from VOL file
        class(amatrix), intent(inout)  :: this
        class(paramlist), intent(in)   :: params
        logical       , intent(in)     :: cdfbool
        character(len=250)             :: pout
        integer                        :: dfile, i, eq, j
        integer                        :: dualid, varid, nall_id, nallpoints
        integer, allocatable           :: locid(:)

        if(cdfbool) then
            if (params%read_netCDF) then
                !! First reading from DUALGRID file
                write(pout,'(a)') "READING LOCAL_ID FROM DUALGRID FILE: " // &
                                    trim(params%dualgrid) // '\n'
                call PetscPrintf(PETSC_COMM_WORLD, trim(pout),ierr)

                ! Get dual grid local ids for sorting
                call check(nf90_open(params%dualgrid, NF90_NOWRITE, dualid))
                call check(nf90_inq_varid(dualid, "local_id", varid))
                call check( nf90_inq_dimid(dualid, 'nallpoints', nall_id))
                call check(nf90_inquire_dimension(dualid, nall_id, len=nallpoints))
                allocate(locid(nallpoints))
                call check( nf90_get_var(dualid, varid, locid) )
                call check( nf90_close(dualid) )

                if (allocated(this%localid)) deallocate(this%localid)
                allocate(this%localid(this%nvar))
                j=0
                do i = 1, this%nvar, this%neq
                    j = j + 1
                    do eq = 0, this%neq-1
                        this%localid(i+eq) = locid(j) ! insert extra values (NEQ)
                    end do
                end do
            ELSE
                if (allocated(this%localid)) deallocate(this%localid)
                allocate(this%localid(this%nvar))
                j=0
                do i = 1, this%nvar, this%neq
                    j = j + 1
                    do eq = 0, this%neq-1
                        this%localid(i+eq) = j ! insert extra values (NEQ)
                    end do
                end do
            END IF

        else
            !! Second reading from permuted ID file, after Domain Reduction
            open(newunit=dfile, file=trim(params%dualgrid), &
                    action='READ', status='OLD')
            if (allocated(this%localid)) deallocate(this%localid)
            allocate(this%localid(this%nvar))
            do i = 1, this%nvar, this%neq
                read(dfile,*) this%localid(i)
                do eq = 1, this%neq-1
                    this%localid(i+eq) = this%localid(i) ! insert extra values
                end do
            end do
            close(dfile)
        end if

    end subroutine read_localid
!! ======================================================================== !!
    subroutine correct_coord_vol_id(this)
        ! Correct coordinates and volumes array, eliminating extra array entries
        class(amatrix), intent(inout) :: this
        real(kind=RP), allocatable    :: auxc(:,:), auxv(:)
        integer, allocatable          :: auxid(:)
        integer :: npoint, pt

        npoint = this%nvar / this%neq

        allocate(auxc(this%nvar, this%ndim), auxv(this%nvar), auxid(this%nvar))
        auxc(:,:) = this%coord(:,:)
        auxv(:)   = this%vol(:)
        auxid(:)  = this%localid(:)
        deallocate(this%vol, this%coord, this%localid)
        allocate(this%vol(npoint), this%coord(npoint, this%ndim), &
                    this%localid(npoint))

        do pt = 1, npoint
            this%vol(pt)     = auxv(pt*this%neq)
            this%coord(pt,:)   = auxc(pt*this%neq,:)
            this%localid(pt) = auxid(pt*this%neq)
        end do

        deallocate(auxc, auxv, auxid)

    end subroutine correct_coord_vol_id
!! ======================================================================== !!
    subroutine read_matrix(this, params)
        ! Read jacobian values from CRS matrix, generating appropiate arrays
        ! Applies correction for NONZERO elements (if any zero stored)
        ! Applies Domain Reduction Technique, if required
        class(amatrix), intent(inout)  :: this
        type(paramlist), intent(inout) :: params
        real(kind=RP)                  :: Aval
        integer                        :: infh, matfh, i, check_nnz, xx
        character(len=250)             :: pout

        write(pout,'(a)') "READING JACOBIAN VALUES FROM AMG FILE: " // &
                            trim(params%matfile) // '\n'
        call PetscPrintf(PETSC_COMM_WORLD, trim(pout),ierr)

        if (rank.eq.0) then
            ! Read Jacobian Matrix
            IF (params%read_netCDF) then
                S = SparseMatrix()
                CALL S % read(params%matfile)
                this%nvar = S%N
                this%nnz = S%nnz
                this%neq = S%neq
            ELSE
                S = SparseMatrix()
                write(pout,'(a)') "READING INFO FROM INFO FILE: " // &
                                trim(params%infofile) // '\n'
                call PetscPrintf(PETSC_COMM_WORLD, trim(pout),ierr)
                ! Open info file
                open(newunit=infh, file=trim(params%infofile), action='READ', status='OLD')
                read(infh,*)
                read(infh,*) this%nnz, this%nvar, xx, this%neq, xx
                close(infh)

                S%N = this%nvar
                S%nnz = this%NNZ
                S%neq = this%neq

                allocate(S%row_ptr(S%N+1))
                allocate(S%col_ind(S%nnz))
                allocate(S%val(S%nnz))

                open(newunit=matfh, file=params%matfile, action='READ',status='OLD')
                ! Read row pointers to obtain number of nonzero elements
                do i = 1, this%nvar+1
                    read(matfh, *) S%row_ptr(i)
                end do

                ! Check if NNZ value from Infofile and number of non zero elements
                ! read from AMG file match
                check_nnz = S%row_ptr(this%nvar+1) - 1
                if ( check_nnz /= this%nnz ) then
                    if (rank .eq. 0) then
                        write (*,*)'  *** WARNING **** : EXPECTED NUMBER OF NONZERO ELEMENTS' &
                                ,S%nnz,' DIFFERS FROM READ NON ZERO ELEMENTS', check_nnz
                        write(*,*) '  *** VALUE HAS BEEN CORRECTED ***'
                    end if
                    S%nnz = check_nnz
                    this%nnz = check_nnz
                endif

                ! Read columns index
                do i = 1, S%nnz
                    read(matfh,*) S%col_ind(i)
                end do

                !Read matrix values
                do i = 1, S%nnz
                    read(matfh,*) Aval
                    S%val(i) = Aval * (1.0d0, 0.0d0)
                end do

                call S%calculate_irn()
            END IF

            call read_coordinates(this, params)
            call read_volumes(this, params)
            call read_localid(this, params, .true.)

            S%ndim = this%ndim

            if (rank.eq. 0) then
                write(*,'(A,I0)')  " VARIABLES  : ", S%N
                write(*,'(A,I0)')  " EQUATIONS  : ", S%neq
                write(*,'(A,I0)')  " DIMENSIONS : ", S%ndim
                write(*,'(A,I0)')  " NNZ ELEMS  : ", S%nnz
                write(*,*); write(*,*) '   DONE'; write(*,*)
            end if

            ! Matrix adjustments (if required)
            if (params%reduce_jac) then
                if (rank .eq. 0) then
                    write(*,*) 'MATRIX REDUCTION METHOD WAS CHOSEN.'
                    write(*,*) 'STARTING PERMUTATION OF JACOBIAN MATRIX...'
                    write(*,*)
                end if

                if (this%ndim .eq. 2) then
                    call permutation(S, this%coord, this%vol, this%localid, &
                                        params%zmin, params%zmax, &
                                        params%xmin, params%xmax)
                else if (this%ndim .eq. 3) then
                    call permutation3D(S, this%coord, this%vol, this%localid, &
                                        params%zmin, params%zmax, &
                                        params%ymin, params%ymax, &
                                        params%xmin, params%xmax)
                else
                    write(*,*) '    You should not be seeing this...'
                    write(*,*) '    NDIM > 3. Sorry, this is a 3D world. Exiting...'
                    stop
                end if

                ! Reassignation of matrix dimensions after domain reduction, and
                ! read coordinates and volume data from permuted files
                this%nvar = S%N
                this%nnz = S%nnz
                params%coordfile = 'newcoor.dat'
                params%volfile = 'newvol.dat'
                params%dualgrid = 'newlocalid.dat'
                call read_coordinates(this, params)
                call read_volumes(this, params)
                call read_localid(this, params, .false.)
            end if

            ! Eliminates extra entries in coordinate and volume arrays
            call correct_coord_vol_id(this)

            ! This will be done automatically on the future, but is kept for
            ! debugging purposes...
            if(params%cleannz) then
                call cleanzeros(S)
                this%nnz = S%nnz
            end if

            ! Apply TAU adimensional correction
            if (params%TAUSCALE) then
                write(pout,*) '\nCORRECTING JAC MATRIX WITH TAU-FACTOR...\n'
                call PetscPrintf(PETSC_COMM_WORLD, trim(pout),ierr)
                do i = 1, S%NNZ
                    S%val(i) = S%val(i) * this%afactor
                enddo
                write(pout,*) '\nDONE.\n'
                call PetscPrintf(PETSC_COMM_WORLD, trim(pout),ierr)
            end if
        end if ! (rank.eq.0)

        ! Broadcast data and allocate arrays in all processors
        if (size.gt.1) then
            call MPI_Bcast(this%nvar, 1, MPI_INT, 0, PETSC_COMM_WORLD, ierr);CHKERRA(ierr)
            call MPI_Bcast(this%neq, 1, MPI_INT, 0, PETSC_COMM_WORLD, ierr);CHKERRA(ierr)
            call MPI_Bcast(this%ndim, 1, MPI_INT, 0, PETSC_COMM_WORLD, ierr);CHKERRA(ierr)
            call MPI_Bcast(this%nnz, 1, MPI_INT, 0, PETSC_COMM_WORLD, ierr);CHKERRA(ierr)
        end if

        allocate(this%row_ptr(this%nvar+1))
        allocate(this%col_ind(this%nnz))
        allocate(this%data(this%nnz))

        ! Get data from first processsor, destroy reading object and broadcast
        if (rank.eq.0) THEN
            this%row_ptr = S%row_ptr
            this%col_ind = S%col_ind
            this%data    = S%val
            DEALLOCATE(S%VAL, S%JCN, S%IRN, S%row_ptr, S%col_ind)
        end if

        ! Allocate volumes vector in the rest of the processors
        if (rank.ne.0) allocate(this%vol(this%nvar/this%neq))

        !! ######################################################
        !! ## DEBUGGING CRS EXAMPLE (FOR DEVELOPMENT)          ##
        !! ## COMMENT THIS FOR RELEASE VERSION OF THE CODE!!   ##
        !! ######################################################
        ! deallocate(this%row_ptrT, this%col_indT, this%dataT)
        ! deallocate(this%row_ptr, this%col_ind, this%data)
        ! deallocate(this%vol)
        ! call MPI_BARRIER(PETSC_COMM_WORLD, ierr)
        ! this%nvar = 6
        ! this%nnz = 19
        ! allocate(this%row_ptr(this%nvar+1), this%col_ind(this%nnz), this%data(this%nnz))
        ! allocate(this%vol(this%nvar))
        ! if (rank.eq.0) then
        !     this%row_ptr = (/1,3,6,9,13,17,20/)
        !     this%col_ind = (/1 ,5 ,1,2,6,2,3,4,1,3,4,5,2,4,5,6 ,2,5,6 /)
        !     this%data    = (/10,-2,3,9,3,7,8,7,3,8,7,5,8,9,9,13,4,2,-1/)
        !     ! this%vol     = (/1,1,1,1,1,1/)
        !     this%vol     = (/1,2,3,4,5,6/)
        ! end if

        !! ######################################################

        if (size.gt.1) then
            call MPI_Bcast(this%row_ptr, this%nvar+1, MPI_INT, 0, PETSC_COMM_WORLD, ierr);CHKERRA(ierr)
            call MPI_Bcast(this%col_ind, this%nnz, MPI_INT, 0, PETSC_COMM_WORLD, ierr);CHKERRA(ierr)
            call MPI_Bcast(this%data, this%nnz, MPIU_SCALAR, 0, PETSC_COMM_WORLD, ierr);CHKERRA(ierr)
            call MPI_Bcast(this%vol, this%nvar/this%neq, MPIU_REAL, 0, PETSC_COMM_WORLD, ierr);CHKERRA(ierr)
        end if

        ! npoint = this%nvar / this%neq
        ! if (.not.allocated(this%vol)) allocate(this%vol(npoint))
        ! if (.not.allocated(this%coord)) allocate(this%coord(npoint, this%ndim))
        ! call MPI_Bcast(this%vol, npoint, MPIU_SCALAR, 0, PETSC_COMM_WORLD, ierr);CHKERRA(ierr)
        ! call MPI_Bcast(this%coord, npoint*this%ndim, MPIU_SCALAR, 0, PETSC_COMM_WORLD, ierr);CHKERRA(ierr)

        call MPI_BARRIER(PETSC_COMM_WORLD, ierr)

    end subroutine read_matrix
!! ======================================================================== !!
    subroutine destroy(this)
        !! Deallocates all the arrays used for Jacobian storage
        class(amatrix), intent(inout) :: this

        if (allocated(this%row_ptr)) deallocate(this%row_ptr)
        if (allocated(this%col_ind)) deallocate(this%col_ind)
        if (allocated(this%data)) deallocate(this%data)
        if (allocated(this%coord)) deallocate(this%coord)
        if (allocated(this%vol)) deallocate(this%vol)

    end subroutine destroy
!! ======================================================================== !!
    function count_coordinates(fileid) result(nvar)
        ! Function to count the number of lines in one file
        ! Used on the coordinates file (COO), gives the number of rows of the JAC
        integer, intent(in) :: fileid
        integer             :: io, nlines, nvar
        nlines = 0
        DO
            READ(fileid,*,IOSTAT=io)
            IF (io > 0) THEN
                PRINT*, 'Check .coo file. Something was wrong.'
            ELSE IF (io < 0) THEN
                nvar = nlines
                EXIT
            ELSE
                nlines = nlines + 1
            END IF
        END DO
    end function count_coordinates
!! ======================================================================== !!
    subroutine check(status)
        ! NetCDF check-error function
        integer, intent ( in) :: status

        if(status /= nf90_noerr) then
            print *, trim(nf90_strerror(status))
            stop "Stopped"
        end if
    end subroutine check
!-------------------------------------------------------------------------------
end module jac_matrix
