module math_tools
    use precision_mod,   only: RP
contains
!-------------------------------------------------------------------------------
    subroutine zamux ( n, x, y, a, ja, ia )
    !***************************************************************************
    !
    !! AMUX multiplies a CSR matrix A times a vector.
    !
    !  Discussion:
    !
    !    This routine multiplies a matrix by a vector using the dot product form.
    !    Matrix A is stored in compressed sparse row storage.
    !
    !  Modified:
    !
    !    07 January 2004
    !
    !  Author:
    !
    !    Youcef Saad
    !
    !  Parameters:
    !
    !    Input, integer ( kind = 4 ) N, the row dimension of the matrix.
    !
    !    Input, real X(*), and array of length equal to the column dimension
    !    of A.
    !
    !    Input, real A(*), integer ( kind = 4 ) JA(*), IA(NROW+1), the matrix in CSR
    !    Compressed Sparse Row format.
    !
    !    Output, real Y(N), the product A * X.
        implicit none

        integer ( kind = 4 ) n

        complex ( kind = RP ) a(*)
        integer ( kind = 4 ) i
        integer ( kind = 4 ) ia(*)
        integer ( kind = 4 ) ja(*)
        integer ( kind = 4 ) k
        complex ( kind = RP ) t
        complex ( kind = RP ) x(*)
        complex ( kind = RP ) y(n)

        do i = 1, n
            !
            !  Compute the inner product of row I with vector X.
            !
            t = 0.0D+00
            do k = ia(i), ia(i+1)-1
                t = t + a(k) * x(ja(k))
            end do

            y(i) = t

        end do

        return
    end subroutine zamux
!-------------------------------------------------------------------------------
    !**********************************************************************
    !*
    !*..Name:
    !*     L2NORM
    !*..Purpose:
    !*     Compute the L2-norm of a vector
    !*..Description:
    !*     Given a vector  vec
    !*     use the f90 intrinsic dot_product to calculate its norm
    !*..Author:
    !*     Vassilios.Theofilis@dlr.de
    !*..Language:
    !*     Fortran 90
    !*..History:
    !*     Original version    1997.09.23
    !*    (enter changes here)
    subroutine L2NORM(n,vec,l2)

        implicit none

        intrinsic sqrt

        integer           :: n
        COMPLEX(kind=RP)  :: vec(1:n)
        COMPLEX(kind=RP)  :: l2
        !  dimension         vec(1:n)
        l2 = 0.0d0
        l2 = dot_product(vec,vec)
        l2 = sqrt(l2)
        return
    end subroutine
!-------------------------------------------------------------------------------
subroutine coocsr ( nrow, nnz, a, ir, jc, ao, jao, iao )
!***************************************************************************
!
!! COOCSR converts COO to CSR.
!
!  Discussion:
!
!    This routine converts a matrix that is stored in COO coordinate format
!    a, ir, jc into a CSR row general sparse ao, jao, iao format.
!
!  Modified:
!
!    07 January 2004
!
!  Author:
!
!    Youcef Saad
!
!  Parameters:
!
!    Input, integer ( kind = 4 ) NROW, the row dimension of the matrix.
!
!    Input, integer ( kind = 4 ) NNZ, the number of nonzero elements.
!
! a,
! ir,
! jc    = matrix in coordinate format. a(k), ir(k), jc(k) store the nnz
!         nonzero elements of the matrix with a(k) = actual real value of
!         the elements, ir(k) = its row number and jc(k) = its column
!        number. The order of the elements is arbitrary.
!
! on return:
!
! ir       is destroyed
!
!    Output, real AO(*), JAO(*), IAO(NROW+1), the matrix in CSR
!    Compressed Sparse Row format.
!
    implicit none

    integer ( kind = 4 ) nrow

    real ( kind = 8 ) a(*)
    real ( kind = 8 ) ao(*)
    integer ( kind = 4 ) i
    integer ( kind = 4 ) iad
    integer ( kind = 4 ) iao(nrow+1)
    integer ( kind = 4 ) ir(*)
    integer ( kind = 4 ) j
    integer ( kind = 4 ) jao(*)
    integer ( kind = 4 ) jc(*)
    integer ( kind = 4 ) k
    integer ( kind = 4 ) k0
    integer ( kind = 4 ) nnz
    real ( kind = 8 ) x

    iao(1:nrow+1) = 0
    !
    !  Determine the row lengths.
    !
    do k = 1, nnz
        iao(ir(k)) = iao(ir(k)) + 1
    end do
    !
    !  The starting position of each row.
    !
    k = 1
    do j = 1, nrow+1
        k0 = iao(j)
        iao(j) = k
        k = k + k0
    end do
    !
    !  Go through the structure once more.  Fill in output matrix.
    !
    do k = 1, nnz
        i = ir(k)
        j = jc(k)
        x = a(k)
        iad = iao(i)
        ao(iad) = x
        jao(iad) = j
        iao(i) = iad + 1
    end do
    !
    !  Shift back IAO.
    !
    do j = nrow, 1, -1
        iao(j+1) = iao(j)
    end do
    iao(1) = 1

    return
end subroutine coocsr
!-------------------------------------------------------------------------------
subroutine amux ( n, x, y, a, ja, ia )
!***************************************************************************
!
!! AMUX multiplies a CSR matrix A times a vector.
!
!  Discussion:
!
!    This routine multiplies a matrix by a vector using the dot product form.
!    Matrix A is stored in compressed sparse row storage.
!
!  Modified:
!
!    07 January 2004
!
!  Author:
!
!    Youcef Saad
!
!  Parameters:
!
!    Input, integer ( kind = 4 ) N, the row dimension of the matrix.
!
!    Input, real X(*), and array of length equal to the column dimension
!    of A.
!
!    Input, real A(*), integer ( kind = 4 ) JA(*), IA(NROW+1), the matrix in CSR
!    Compressed Sparse Row format.
!
!    Output, real Y(N), the product A * X.
    implicit none

    integer ( kind = 4 ) n

    real ( kind = 8 ) a(*)
    integer ( kind = 4 ) i
    integer ( kind = 4 ) ia(*)
    integer ( kind = 4 ) ja(*)
    integer ( kind = 4 ) k
    real ( kind = 8 ) t
    real ( kind = 8 ) x(*)
    real ( kind = 8 ) y(n)

    do i = 1, n
        !
        !  Compute the inner product of row I with vector X.
        !
        t = 0.0D+00
        do k = ia(i), ia(i+1)-1
            t = t + a(k) * x(ja(k))
        end do

        y(i) = t

    end do

    return
end subroutine amux
!-------------------------------------------------------------------------------
subroutine amub ( nrow, ncol, job, a, ja, ia, b, jb, ib, c, jc, ic, nzmax, &
    iw, ierr )
!***************************************************************************
!
!! AMUB performs the matrix product C = A * B.
!
!  Discussion:
!
!    The column dimension of B is not needed.
!
!  Modified:
!
!    08 January 2004
!
!  Author:
!
!    Youcef Saad
!
!  Parameters:
!
!    Input, integer ( kind = 4 ) NROW, the row dimension of the matrix.
!
!    Input, integer ( kind = 4 ) NCOL, the column dimension of the matrix.
!
!    Input, integer ( kind = 4 ) JOB, job indicator.  When JOB = 0, only the
!    structure is computed, that is, the arrays JC and IC, but the real values
!    are ignored.
!
!    Input, real A(*), integer ( kind = 4 ) JA(*), IA(NROW+1), the matrix in CSR
!    Compressed Sparse Row format.
!
!    Input, b, jb, ib, matrix B in compressed sparse row format.
!
!    Input, integer ( kind = 4 ) NZMAX, the length of the arrays c and jc.
!    The routine will stop if the result matrix C  has a number
!    of elements that exceeds exceeds NZMAX.
!
! on return:
!
! c,
! jc,
! ic    = resulting matrix C in compressed sparse row sparse format.
!
! ierr      = integer ( kind = 4 ). serving as error message.
!         ierr = 0 means normal return,
!         ierr > 0 means that amub stopped while computing the
!         i-th row  of C with i = ierr, because the number
!         of elements in C exceeds nzmax.
!
! work arrays:
!
!  iw      = integer ( kind = 4 ) work array of length equal to the number of
!         columns in A.
    implicit none

    integer ( kind = 4 ) ncol
    integer ( kind = 4 ) nrow
    integer ( kind = 4 ) nzmax

    real ( kind = 8 ) a(*)
    real ( kind = 8 ) b(*)
    real ( kind = 8 ) c(nzmax)
    integer ( kind = 4 ) ia(nrow+1)
    integer ( kind = 4 ) ib(ncol+1)
    integer ( kind = 4 ) ic(ncol+1)
    integer ( kind = 4 ) ierr
    integer ( kind = 4 ) ii
    integer ( kind = 4 ) iw(ncol)
    integer ( kind = 4 ) ja(*)
    integer ( kind = 4 ) jb(*)
    integer ( kind = 4 ) jc(nzmax)
    integer ( kind = 4 ) jcol
    integer ( kind = 4 ) jj
    integer ( kind = 4 ) job
    integer ( kind = 4 ) jpos
    integer ( kind = 4 ) k
    integer ( kind = 4 ) ka
    integer ( kind = 4 ) kb
    integer ( kind = 4 ) len
    real ( kind = 8 ) scal
    logical values

    values = ( job /= 0 )
    len = 0
    ic(1) = 1
    ierr = 0
    !
    !  Initialize IW.
    !
    iw(1:ncol) = 0

    do ii = 1, nrow
        !
        !  Row I.
        !
        do ka = ia(ii), ia(ii+1)-1

            if ( values ) then
                scal = a(ka)
            end if

            jj = ja(ka)

            do kb = ib(jj), ib(jj+1)-1

                jcol = jb(kb)
                jpos = iw(jcol)

                if ( jpos == 0 ) then
                    len = len + 1
                    if ( nzmax < len ) then
                        ierr = ii
                        return
                    end if
                    jc(len) = jcol
                    iw(jcol)= len
                    if ( values ) then
                        c(len) = scal * b(kb)
                    end if
                else
                    if ( values ) then
                        c(jpos) = c(jpos) + scal * b(kb)
                    end if
                end if

            end do

        end do

        do k = ic(ii), len
            iw(jc(k)) = 0
        end do

        ic(ii+1) = len + 1

    end do

    return
end subroutine amub
!-------------------------------------------------------------------------------
subroutine csrcsc ( n, job, ipos, a, ja, ia, ao, jao, iao )
!*****************************************************************************80
!
!! CSRCSC converts Compressed Sparse Row to Compressed Sparse Column.
!
!  Discussion:
!
!    This is essentially a transposition operation.
!
!    It is NOT an in-place algorithm.
!
!    This routine transposes a matrix stored in a, ja, ia format.
!
!  Modified:
!
!    07 January 2004
!
!  Author:
!
!    Youcef Saad
!
!  Parameters:
!
!    Input, integer ( kind = 4 ) N, the order of the matrix.
!
!    Input, integer ( kind = 4 ) JOB, indicates whether or not to fill the values
!    of the matrix AO or only the pattern (IA, and JA).  Enter 1 for yes.
!
! ipos  = starting position in ao, jao of the transposed matrix.
!         the iao array takes this into account (thus iao(1) is set to ipos.)
!         Note: this may be useful if one needs to append the data structure
!         of the transpose to that of A. In this case use
!                call csrcsc (n,1,n+2,a,ja,ia,a,ja,ia(n+2))
!        for any other normal usage, enter ipos=1.
!
!    Input, real A(*), integer ( kind = 4 ) JA(*), IA(N+1), the matrix in CSR
!    Compressed Sparse Row format.
!
!    Output, real AO(*), JAO(*), IAO(N+1), the matrix in CSC
!    Compressed Sparse Column format.
!
    implicit none

    integer ( kind = 4 ) :: n

    complex ( kind = RP ) :: a(*)
    complex ( kind = RP ) :: ao(*)
    integer ( kind = 4 )  :: i
    integer ( kind = 4 )  :: ia(n+1)
    integer ( kind = 4 )  :: iao(n+1)
    integer ( kind = 4 )  :: ipos
    integer ( kind = 4 )  :: j
    integer ( kind = 4 )  :: ja(*)
    integer ( kind = 4 )  :: jao(*)
    integer ( kind = 4 )  :: job
    integer ( kind = 4 )  :: k
    integer ( kind = 4 )  :: next
    !
    !  Compute lengths of rows of A'.
    !
    iao(1:n+1) = 0

    do i = 1, n
        do k = ia(i), ia(i+1)-1
            j = ja(k) + 1
            iao(j) = iao(j) + 1
        end do
    end do
    !
    !  Compute pointers from lengths.
    !
    iao(1) = ipos
    do i = 1, n
        iao(i+1) = iao(i) + iao(i+1)
    end do
    !
    !  Do the actual copying.
    !
    do i = 1, n
        do k = ia(i), ia(i+1)-1
            j = ja(k)
            next = iao(j)
            if ( job == 1 ) then
                ao(next) = a(k)
            end if
            jao(next) = i
            iao(j) = next + 1
        end do
    end do
    !
    !  Reshift IAO and leave.
    !
    do i = n, 1, -1
        iao(i+1) = iao(i)
    end do
    iao(1) = ipos

    return
end subroutine csrcsc
!-------------------------------------------------------------------------------
end module math_tools
