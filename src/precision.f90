module precision_mod
    implicit none

    integer, parameter :: RP = SELECTED_REAL_KIND (15, 307)      ! Double kind

    contains

    elemental logical function isequal(a,b)
        real(kind = RP), intent(in)   :: a,b

        if ( abs(a - b) < 10._RP * epsilon(1._RP)) then
            isequal = .true.
        else
            isequal = .false.
        endif

    end function

end module
