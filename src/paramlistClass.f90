module paramlistClass
#include <petsc/finclude/petscsys.h>
    use precision_mod, only: RP
    use petscsys
    implicit none
!! ---------------------------------------------------------------------!!
    type line
        character(len = 100) :: label
        character(len = 100) :: value
    end type line
!! ---------------------------------------------------------------------!!
    type paramlist
        type(line), allocatable, dimension(:)     :: entries
        character(len = 100)                      :: filename
        integer, private                          :: nlines
        integer, private                          :: ndatalines

        !! List of implemented parameters
        character(len=250)                        :: workingdir
        character(len=250)                        :: matfile, volfile
        character(len=250)                        :: hkinfile, hkoutfile
        character(len=250)                        :: coordfile, infofile
        character(len=250)                        :: eigv_file, eigf_path
        character(len=250)                        :: dualgrid
        character(len=10)                         :: factorization
        character(len=2)                          :: which   ! ARPACK Type of eigenvalue requested
        logical                                   :: load_hk, save_hk
        logical                                   :: read_netCDF, spval, tauscale
        logical                                   :: comp_eigf
        logical                                   :: reduce_jac, cleannz
        logical                                   :: use_arpack
        logical                                   :: statistics
        logical                                   :: generalized, scalejmatrix
        real(kind=RP)                             :: mach, rlength
        real(kind=RP)                             :: zmin, zmax, ymin, ymax, xmin, xmax
        real(kind=RP)                             :: tol    ! ARPACK tolerance
        complex(kind=RP)                          :: shift  ! Shift Arnoldi parameter
        integer                                   :: nev    ! ARPACK number of Eigenvalues
        integer                                   :: ncv    ! Projected subspace dimension

    contains
        procedure, private   :: get_item
        procedure            :: get_nlines
        procedure            :: get_ndatalines
        procedure, private   :: assign_default_values
        procedure            :: read_params
    end type paramlist

    interface paramlist
        module procedure new_list
    end interface

    private
    public   paramlist

    contains
!-------------------------------------------------------------------------------
    function new_list(filename, isep)
        ! Constructor for the PARAMETERS object, containing all the information
        ! of the 'param.txt' file
        type(paramlist)      :: new_list
        character(len=*)     :: filename
        character,optional   :: isep               ! default separator '='

        integer              :: funit, nlines, ndatalines, io, i, pos, idx
        character(len = 200) :: buffer
        character            :: separator

        new_list%filename = filename

        if(.not. present(isep)) then
            separator = "="   ! default separator
        else
            separator = isep
        endif

        open(newunit=funit,file=filename)

        ! read number of lines in the file
        nlines = 0
        ndatalines = 0
        do
            read(funit,'(A)',iostat=io) buffer
            if (io < 0) exit
            if (index(buffer, "#") == 0 .and. index(buffer, separator)/=0) then
                ndatalines = ndatalines + 1 ! valid data lines
            end if
            nlines = nlines + 1
        end do
        rewind(funit)

        allocate (new_list%entries(ndatalines))
        idx = 0
        do i = 1, nlines                                            ! read the file line by line
            read(funit, '(A)') buffer                                ! write the entire file to buffer
            pos = index(buffer, "#")                                 ! check if the line is commented
            if (pos == 0) then                                       ! if the line is not commented look for separator
                pos = index(buffer, separator)                        ! locate the position of the separator
                if (pos /= 0) then                                    ! if pos = 0, the line contains no separator
                    idx = idx + 1
                    new_list%entries(idx)%label = trim(buffer(:pos-1))   ! label is the line up to the separator
                    new_list%entries(idx)%value = trim(buffer(pos+1:))   ! values is the line from the separator
                end if
            endif
        end do
        close(funit)
        new_list%nlines = nlines
        new_list%ndatalines = ndatalines

        call assign_default_values(new_list)

    end function new_list
!-------------------------------------------------------------------------------
    function get_nlines(this) result(lines)
        class(paramlist)  :: this
        integer           :: lines
        lines = this%nlines
    end function
!-------------------------------------------------------------------------------
    function get_ndatalines(this) result(lines)
        class(paramlist)  :: this
        integer           :: lines
        lines = this%ndatalines
    end function
!-------------------------------------------------------------------------------
    function search_value(this, label) result(value)
        class(paramlist)  :: this
        character(len=*)  :: label
        character(len=100):: value
        character(len=250):: pout
        integer           :: i, ierr

        do i = 1, size(this%entries)
            if (label == this%entries(i)%label) then
                value = this%entries(i)%value
                return
            end if
        end do
        write(pout,'(A)') ' Label ['//trim(label)//'] not found in param file\n'// &
                            ' Using default value.\n'
        call PetscPrintf(PETSC_COMM_WORLD, trim(pout), ierr);CHKERRA(ierr)
        write(value,'(a)') "default"

    end function
!-------------------------------------------------------------------------------
    subroutine get_item(this, label, value)
        ! Search for item in PARA file, and clasify it depending on its type.
        class(paramlist), intent(inout) :: this
        character(len=*), INTENT(IN)    :: label
        class(*),         INTENT(INOUT) :: value
        character(len=250)              :: value_str

        value_str = search_value(this,label)
        if (value_str .eq. "default") then
            return
        end if

        select type (value)
        type is (real(4))
            read(value_str,*) value
        type is (real(8))
            read(value_str,*) value
        type is (complex(8))
            read(value_str,*) value
        type is (integer)
            read(value_str,*) value
        type is (character(*))
            read(value_str,*) value
        type is (logical)
            read(value_str,*) value
        end select
    end subroutine get_item
!-------------------------------------------------------------------------------
    subroutine assign_default_values(this)
        class(paramlist) :: this
        this%workingdir = './'
        this%tauscale = .true.
        this%read_netCDF = .true.
        this%spval = .true.
        this%comp_eigf = .false.
        this%mach = 0.1d0
        this%rlength = 1d0
        this%comp_eigf = .false.
        this%factorization = 'LU'
        this%statistics = .false.
        this%generalized = .true.
        this%scalejmatrix = .false.
        this%use_arpack = .true.
        this%shift = (0.0d0, 0.0d0)
        this%ncv = 61
        this%nev = 20
        this%which = 'LM'
        this%tol = 1d-6
        this%load_hk = .false.
        this%save_hk = .false.
        this%cleannz = .true.
        this%reduce_jac = .false.
        this%xmin = 0d0
        this%xmax = 1d0
        this%ymin = 0d0
        this%ymax = 1d0
        this%zmin = 0d0
        this%zmax = 1d0
        this%hkinfile = 'DATA/hk.mat'
        this%hkoutfile = 'DATA/hk.mat'
        this%matfile = 'DATA/samg.matrix.amg'
        this%infofile = 'DATA/samg.matrix.frm'
        this%coordfile = 'DATA/samg.matrix.coo'
        this%volfile = 'DATA/samg.matrix.vol'
        this%dualgrid = 'DATA/dualgrid2D'
        this%eigv_file = 'RESULTS/eigv.dat'
        this%eigf_path = 'RESULTS/eigf'
    end subroutine assign_default_values
!-------------------------------------------------------------------------------
    subroutine read_params(this)
        class(paramlist) :: this
        !! Searchs through the pre-read param file, and assigns the values on the
        !! parameters file. If they are not present, keeps the default value
        call get_item(this, 'WORKING_DIR', this%workingdir)
        call get_item(this, 'TAU_SCALING', this%tauscale)
        call get_item(this, 'READ_NETCDF', this%read_netCDF)
        call get_item(this, 'SAVE2PVAL', this%spval)
        call get_item(this, 'COMPUTE_EIGENVECTORS', this%comp_eigf)
        call get_item(this, 'MACH', this%mach)
        call get_item(this, 'RE_LENGTH', this%rlength)
        call get_item(this, 'COMPUTE_EIGENVECTORS', this%comp_eigf)
        call get_item(this, 'FACTORIZATION', this%factorization)
        call get_item(this, 'STATISTICS', this%statistics)
        call get_item(this, 'GENERALIZED', this%generalized)
        call get_item(this, 'SCALEJMATRIX', this%scalejmatrix)
        call get_item(this, 'USE_ARPACK', this%use_arpack)
        call get_item(this, 'SHIFT', this%shift)
        call get_item(this, 'NCV', this%ncv)
        call get_item(this, 'NEV', this%nev)
        call get_item(this, 'ARPACK_EGV_SELECTION', this%which)
        call get_item(this, 'TOL', this%tol)
        call get_item(this, 'LOAD_HK', this%load_hk)
        call get_item(this, 'SAVE_HK', this%save_hk)
        call get_item(this, 'CLEANZEROS', this%cleannz)
        call get_item(this, 'REDUCE_JAC', this%reduce_jac)
        call get_item(this, 'XMIN', this%xmin)
        call get_item(this, 'XMAX', this%xmax)
        call get_item(this, 'YMIN', this%ymin)
        call get_item(this, 'YMAX', this%ymax)
        call get_item(this, 'ZMIN', this%zmin)
        call get_item(this, 'ZMAX', this%zmax)
        call get_item(this, 'HK_INFILE', this%hkinfile)
        call get_item(this, 'HK_OUTFILE', this%hkoutfile)
        call get_item(this, 'MATFILE', this%matfile)
        call get_item(this, 'INFOFILE', this%infofile)
        call get_item(this, 'COORDFILE', this%coordfile)
        call get_item(this, 'VOLFILE', this%volfile)
        call get_item(this, 'DUALGRID', this%dualgrid)
        call get_item(this, 'EIGV_FILE', this%eigv_file)
        call get_item(this, 'EIGF_PATH',this%eigf_path)

        !! Paths correction
        this%eigv_file = trim(this%workingdir)//trim(this%eigv_file)
        this%eigf_path = trim(this%workingdir)//trim(this%eigf_path)
        this%dualgrid  = trim(this%workingdir)//trim(this%dualgrid)
        this%coordfile = trim(this%workingdir)//trim(this%coordfile)
        this%volfile = trim(this%workingdir)//trim(this%volfile)
        this%matfile = trim(this%workingdir)//trim(this%matfile)
        this%infofile = trim(this%workingdir)//trim(this%infofile)
        this%hkinfile = trim(this%workingdir)//trim(this%hkinfile)
        this%hkoutfile = trim(this%workingdir)//trim(this%hkinfile)

        !! Solver checks (better now than later...)
        if (this%use_arpack .and. this%nev.gt.this%ncv) then
            print*, 'NCV = ', this%ncv
            print*, 'NEV = ', this%nev
            write(*,*) 'Krylov Subspace is smaller than number of eigenvalues requested!'
            write(*,*) 'Stopping program...'
            write(*,*)
            stop
        end if

    end subroutine read_params
!-------------------------------------------------------------------------------
end module paramlistClass
