############################################
##                                        ##
##                 zTAUev                 ##
##                                        ##
##  Global Stability Solver for DLR-TAU   ##
##                                        ##
############################################
## Author: Alejandro Martinez-Cava (UPM)  ##
## Last review: 29 Jan 2018               ##
############################################

# Requirements:

    - PETSc: Version 3.8 (recommended)
        --> Compiled with SCALAPACK, METIS, PARMETIS, SCOTCH and MUMPS.
        --> To avoid compilation problems, it is recommended to ask PETSc to download
            those libraries during the configuration step.
        --> Own installation of MPI can be used
        --> BLAS & LAPACK libraries should be provided (recommended Intel MKL*)

    - ARPACK

    - NETCDF-Fortran libraries

    - *: Intel MKL libraries: Lapack, Blas, OpenMP

## Set up Make.inc file properly, or generate a new one and modify Makefile
## Use latest param.txt file from the repository, some parameters may have changed!!

# Recommendations (and tips):

    - Before running, generate two folders:
        --> DATA: Store or link here TAU Jacobian files (AMG, COO, FRM) and dualgrid
        --> RESULTS: To store eigenvalues and eigenvectors

    - Set up parameters file PROPERLY. Errors frequently involve WORKING_DIR,
      USE_ARPACK, SHIFT, domain reduction or restarting options.

    - All folders can be also changed in the parameters file.

    - To run, either use MPIEXEC or just call the program path, optionally followed by the parameters file
        --> $ $(zTAUev_PATH)/bin/zTAUev [param.txt]
        --> $ mpiexec -n [NP] $(zTAUev_PATH)/bin/zTAUev [param.txt]

    - ARPACK gives good results for a good shift preconditioning, requiring few eigenvalues (<30)
    - ARNOLDI is less precise, but quicker
    - Use shift&invert preconditioning smartly!!

    - CLEAN_ZEROS option is recommended for efficiency
    - Domain reduction technique can be very efficient, but can also dramatically alter the results (see bibliography)

    - If restarting options are used, remember to keep the method (ARPACK/ARNOLDI) and the number of eigenvalues requested fixed.

    - Direct Solver (LU using MUMPS) or Iterative Solver (PETSc GMRES) can be used, but
      GMRES will need extra parameters (see PETSc User Guide for more information).
