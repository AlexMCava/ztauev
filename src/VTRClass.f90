module VTRClass

   implicit none
   
   type vtr_file
      integer                 :: funit
      character(len= 400 )    :: file
      character(len = 5)      :: action
      integer                 :: mesh_size(3)
      integer                 :: ndataline
      logical                 :: fileisopen
      logical                 :: isheader
      logical                 :: ispointheader
      logical                 :: ismesh
    contains
         procedure :: open => open_vtr_file
         procedure :: close => close_vtr_file
         procedure,private :: write_mesh_3d_double
         procedure,private :: write_data_3d_double
         procedure,private :: write_mesh_3d_single
         procedure,private :: write_data_3d_single
         generic           :: write_mesh => write_mesh_3d_double, write_mesh_3d_single
         generic           :: write_data => write_data_3d_double, write_data_3d_single
         procedure :: read_mesh => read_mesh_3d
         procedure :: read_data => read_data_3d
   end type vtr_file

   private
   public vtr_file

   interface vtr_file
      module procedure new_vtr_file
   end interface vtr_file
   
   interface write_array_ascii
      module procedure write_array_ascii_double, write_array_ascii_single
   end interface
   
   !Module variables
   integer, parameter   :: SP = selected_real_kind(6, 37)
   integer, parameter   :: DP = selected_real_kind(15, 307)
   character(2)         :: tab = "  "
   
   contains
   
   function new_vtr_file(filename, action, ndataline) result(new)
   
      character(len=*)        :: filename
      character(len=*)        :: action
      integer, optional       :: ndataline
      type(vtr_file)          :: new

      if ((trim(action)=="READ") .or. (trim(action)=="WRITE")) then
         new%action = trim(action)
      else
         print*, "ERROR creating vtr_file, action must be WRITE or READ, current value:", trim(action)
         stop
      endif         
      new%file          = trim(filename)
      new%fileisopen    = .false.
      new%isheader      = .false.
      new%ispointheader = .false.
      new%ismesh        = .false.
      
      if (present(ndataline)) then
         new%ndataline = ndataline
      else
         new%ndataline = -1
      endif
       
   end function new_vtr_file
!---------------------------------------------------------------------------------------------------
   subroutine open_vtr_file(this) 
      class(vtr_file), intent(inout)   :: this
      
      if (this%fileisopen) then
         print*, "ERROR opening VTR file, file is already open"
         stop
      else
         if (this%action == "READ") then
            open(newunit=this%funit, file=trim(this%file), status="OLD", action="READ")
         else 
            open(newunit=this%funit, file=trim(this%file), status="UNKNOWN", action="WRITE")
            ! write file header
            write(this%funit,"(A)") '<VTKFile type="RectilinearGrid" version="0.1" format="ascii">'
         endif 
      endif
      this%fileisopen = .true.
      
   end subroutine open_vtr_file
!-------------------------------------------------------------------------------------------------------
   subroutine write_array_ascii_double (funit, array,name, ilevel)
      integer, intent(in)              :: funit
      real(kind = DP), intent(in)      :: array(1:)
      character(len=*), intent(in)     :: name
      integer, intent(in)              :: ilevel
      
      write(funit,"(A)", advance="NO") repeat(tab,ilevel)//'<DataArray type="Float32" Name="'//trim(name)
      write(funit,"(A)")'" NumberOfComponents="1" format="ascii"> '
      write(funit,*) real(array, kind=sp)
      write(funit,"(A)") repeat(tab,ilevel)//'</DataArray>'
         
   end subroutine
!-------------------------------------------------------------------------------------------------------
   subroutine write_array_ascii_single (funit, array,name, ilevel)
      integer, intent(in)              :: funit
      real(kind = SP), intent(in)      :: array(1:)
      character(len=*), intent(in)     :: name
      integer, intent(in)              :: ilevel
      
      write(funit,"(A)", advance="NO") repeat(tab,ilevel)//'<DataArray type="Float32" Name="'//trim(name)
      write(funit,"(A)")'" NumberOfComponents="1" format="ascii"> '
      write(funit,*) real(array, kind=sp)
      write(funit,"(A)") repeat(tab,ilevel)//'</DataArray>'
         
   end subroutine
!-------------------------------------------------------------------------------------------------------
   subroutine read_mesh_3d(this, x,y,z)
      class(vtr_file), intent(inout)                :: this
      real(kind = DP), intent(inout), allocatable   :: x(:),y(:),z(:)
      
      character(len=500)               :: line1 
      integer                          :: reason, first, last, bounds(6)
      
      !Locate mesh within file
      rewind(unit=this%funit)
      do
         read(this%funit, *, IOSTAT=reason) line1
         if (line1 == "<RectilinearGrid") exit    
         if (reason<0) then
            print*, "EOF reached while reading mesh data"
            stop   
         end if
      end do
      
      ! Read mesh sizes
      backspace(unit=this%funit)
      read(this%funit, "(A)" ) line1
      first = index(line1, '"')
      last = index (line1, '"',.true.)
      read(line1(first+1:last-1),*) bounds
      this%mesh_size = [bounds(2)-bounds(1)+1, bounds(4)-bounds(3)+1, bounds(6)-bounds(5)+1 ]
      read(this%funit,*)
      read(this%funit,*)
      
      !allocate coordinate vectors
      if (allocated(x)) deallocate(x)
      if (allocated(y)) deallocate(y)
      if (allocated(z)) deallocate(z)
      
      allocate(x(this%mesh_size(1)))
      allocate(y(this%mesh_size(2)))
      allocate(z(this%mesh_size(3)))
      
      !Read x  coord
      read(this%funit,*)
      read(this%funit,*) x
      read(this%funit,*)
      !Read y  coord
      read(this%funit,*)
      read(this%funit,*) y
      read(this%funit,*)
      !Read z  coord
      read(this%funit,*)
      read(this%funit,*) z
      read(this%funit,*)    
      
      !Read mesh footer
      read(this%funit,*) 
      
   end subroutine
!-------------------------------------------------------------------------------------------------------
   subroutine read_data_3d(this, field, name)
      class(vtr_file), intent(inout)          :: this
      real(kind = DP), intent(inout)          :: field(1:,1:,1:)
      character(len = *), intent(in)          :: name
      
      real(kind = DP), allocatable     :: buf(:)
      integer                          :: reason, npos
      character(len=500)               :: line1 
      
      allocate(buf(size(field)))
      
      ! locate point data within file
      read(this%funit, *, IOSTAT=reason) line1 
      if (line1 == "<PointData>") then
         continue
      else
         rewind(this%funit)     
         do
            read(this%funit, *, IOSTAT=reason) line1
            if (line1 == "<PointData>") exit    
            if (reason<0) then
               print*, "EOF reached while reading mesh data"
               stop   
            end if
         end do
      endif
      
      ! Search point data name and read data
      do 
         read(this%funit, "(A)", iostat=reason ) line1
         if (reason<0) then
            print*, "EOF reached, data label [", trim(name),"] not found in file:", this%file
            stop   
         end if
         npos = index(line1, 'Name="'//trim(name)//'"')
         if (npos == 0) then                  ! name not found in current line
            cycle
         else                                ! name found in current line
            read(this%funit, *) buf          ! read array data into 1d vector buffer
            exit
         end if
      end do
      
      !copy buf data into field array
      field= reshape(buf, [size(field,1),size(field,2),size(field,3)])
      
      deallocate(buf)
   
   end subroutine read_data_3d   
!-------------------------------------------------------------------------------------------------------
   subroutine write_mesh_3d_double(this, x,y,z)
      class(vtr_file), intent(inout)   :: this
      real(kind = DP), intent(in)      :: x(1:)
      real(kind = DP), intent(in)      :: y(1:)
      real(kind = DP), intent(in)      :: z(1:)
      
      integer  :: nx, ny, nz
      nx = size(x)
      ny = size(y)
      nz = size(z) 
      this%mesh_size = [nx, ny, nz]
      
      ! write mesh/data header
      write(this%funit,"(A,6I6,A)") tab//'<RectilinearGrid WholeExtent="',1, nx, 1, ny, 1, nz,'">'
      write(this%funit,"(A,6I6,A)") tab//tab//'<Piece Extent="',1, nx, 1, ny, 1, nz,'">'  
      write(this%funit,"(A)") tab//tab//tab//'<Coordinates>'  
 
      !write mesh components
      call write_array_ascii (this%funit, x,"x_coordinate", 4)
      call write_array_ascii (this%funit, y,"y_coordinate", 4)
      call write_array_ascii (this%funit, z,"z_coordinate", 4)
      
      ! write mesh footer
      write(this%funit,"(A)") tab//tab//tab//'</Coordinates>'
      
      this%ismesh = .true.
              
   end subroutine write_mesh_3d_double
!-------------------------------------------------------------------------------------------------------
   subroutine write_mesh_3d_single(this, x,y,z)
      class(vtr_file), intent(inout)   :: this
      real(kind = SP), intent(in)      :: x(1:)
      real(kind = SP), intent(in)      :: y(1:)
      real(kind = SP), intent(in)      :: z(1:)
      
      integer  :: nx, ny, nz
      nx = size(x)
      ny = size(y)
      nz = size(z) 
      this%mesh_size = [nx, ny, nz]
      
      ! write mesh/data header
      write(this%funit,"(A,6I6,A)") tab//'<RectilinearGrid WholeExtent="',1, nx, 1, ny, 1, nz,'">'
      write(this%funit,"(A,6I6,A)") tab//tab//'<Piece Extent="',1, nx, 1, ny, 1, nz,'">'  
      write(this%funit,"(A)") tab//tab//tab//'<Coordinates>'  
 
      !write mesh components
      call write_array_ascii (this%funit, x,"x_coordinate", 4)
      call write_array_ascii (this%funit, y,"y_coordinate", 4)
      call write_array_ascii (this%funit, z,"z_coordinate", 4)
      
      ! write mesh footer
      write(this%funit,"(A)") tab//tab//tab//'</Coordinates>'
      
      this%ismesh = .true.
              
   end subroutine write_mesh_3d_single
!-------------------------------------------------------------------------------------------------------   
   subroutine write_data_3d_double(this, array, name)
      class(vtr_file), intent(inout)       :: this
      real(kind = DP), intent(in)          :: array(:,:,:)
      character(len=*), intent(in)         :: name
      
      integer                          :: nx, ny, nz
      
      !check is mesh is already written
      if(.not. this%ismesh) then
         print*, "ERROR writting VTR data. Mesh data must be written before point data "
         stop
      endif
      
      !check if data header is already written
      if(.not. this%ispointheader) then
         write(this%funit,"(A)") tab//tab//tab//'<PointData>'
         this%ispointheader = .true.
      endif
      
      ! check array sizes
      nx = size(array, dim=1)
      ny = size(array, dim=2)
      nz = size(array, dim=3)
      
      if ( (nx .ne. this%mesh_size(1)) .or. (ny .ne. this%mesh_size(2)) .or. (nz .ne. this%mesh_size(3)) ) then
         print*, "ERROR writting vtr file:"
         print*, "mesh size [ ",this%mesh_size, "] differs from data point size[", nx,ny,nz,"]"
         stop      
      endif
      
      !write array to file
      call write_array_ascii (this%funit, reshape(array,[nx*ny*nz]) ,trim(name), 4)
      
   end subroutine write_data_3d_double
!-------------------------------------------------------------------------------------------------------   
   subroutine write_data_3d_single(this, array, name)
      class(vtr_file), intent(inout)       :: this
      real(kind = SP), intent(in)          :: array(:,:,:)
      character(len=*), intent(in)         :: name
      
      integer                          :: nx, ny, nz
      
      !check is mesh is already written
      if(.not. this%ismesh) then
         print*, "ERROR writting VTR data. Mesh data must be written before point data "
         stop
      endif
      
      !check if data header is already written
      if(.not. this%ispointheader) then
         write(this%funit,"(A)") tab//tab//tab//'<PointData>'
         this%ispointheader = .true.
      endif
      
      ! check array sizes
      nx = size(array, dim=1)
      ny = size(array, dim=2)
      nz = size(array, dim=3)
      
      if ( (nx .ne. this%mesh_size(1)) .or. (ny .ne. this%mesh_size(2)) .or. (nz .ne. this%mesh_size(3)) ) then
         print*, "ERROR writting vtr file:"
         print*, "mesh size [ ",this%mesh_size, "] differs from data point size[", nx,ny,nz,"]"
         stop      
      endif
      
      !write array to file
      call write_array_ascii (this%funit, reshape(array,[nx*ny*nz]) ,trim(name), 4)
      
   end subroutine write_data_3d_single
!-------------------------------------------------------------------------------------------------------
   subroutine close_vtr_file(this)  
      class(vtr_file), intent(inout)   :: this
      
      if (this%action == "WRITE") then
         !write xml file footer
         write(this%funit,"(A)") tab//tab//tab//'</PointData>'
         write(this%funit,"(A)") tab//tab//'</Piece>'
         write(this%funit,"(A)") tab//'</RectilinearGrid>'
         write(this%funit,"(A)") '</VTKFile>'
      endif
      
      !close file
      close(this%funit)
      this%fileisopen = .false.
   end subroutine close_vtr_file


end module VTRClass
