############################################################################
##        Make.inc for zTAUev software
##
##        Changes should be done here for new installations
##
##        Author: Alejandro Martínez-Cava (UPM)
##        Last revision: 4 Dec 2017
############################################################################

# PETSc & MKL main paths
MKLROOT = /home/alejandro/intel/compilers_and_libraries/linux/mkl
PETSC_DIR = /home/alejandro/Applications/PETSC/COMPLEX

# PETSc
LIB_PETSC = -Wl,-rpath,${PETSC_DIR}/lib -L${PETSC_DIR}/lib -lmetis -lparmetis
PETSC_INC = -I${PETSC_DIR}/include

# EGV & MATH LIBRARIES
LIB_BLAS_LAPACK = ${MKLROOT}/lib/intel64/libmkl_blas95_lp64.a ${MKLROOT}/lib/intel64/libmkl_lapack95_lp64.a -L$(MKLROOT)/lib/intel64 -Wl,--no-as-needed -lmkl_gf_lp64 -lmkl_intel_thread -lmkl_core  -lpthread -lm -ldl -L/home/alejandro/intel/lib/intel64 -liomp5
#LIB_ARPACK = -L/usr/lib -larpack

#LIB_BLAS_LAPACK = ${PETSC_DIR}/lib/libfblas.a ${PETSC_DIR}/lib/libflapack.a
LIB_ARPACK = ${PETSC_DIR}/lib/libarpack.a

# Fortran-netCDF lib
LIB_NETCDFF = -L/usr/lib/x86_64-linux-gnu -lnetcdff
NETCDF_INC = -I/usr/include
