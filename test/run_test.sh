echo ""
echo "Running test in serial mode..."
echo ""
../bin/zTAUev param_arpack.txt
../bin/zTAUev param_arnoldi.txt

echo ""
echo " *********************************** "
echo ""
echo "Running test in parallel mode..."
echo ""
mpiexec -n 2 ../bin/zTAUev param_arpack_MPI.txt
mpiexec -n 2 ../bin/zTAUev param_arnoldi_MPI.txt
echo ""
