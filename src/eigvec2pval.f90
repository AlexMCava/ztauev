module eigvec2pval
    use netcdf
    use precision_mod, only: RP
    implicit none

contains

    subroutine savepval(dualgrid, outfile, eigvec, error, ndim, neq, localid)
        !! Save eigenmode to NetCDF Format.
        !! Requires the TAU Dual Grid to get local_id index information.
        character(len=*),    intent(in)  :: dualgrid
        character(len=*),    intent(in)  :: outfile
        complex(kind=RP),    intent(in)  :: eigvec(:,:)
        complex(kind=RP),    intent(in)  :: error(:,:)
        integer,             intent(in)  :: ndim, neq
        integer,             intent(in)  :: localid(1:)

        real(kind = RP), allocatable  :: var(:)
        integer                       :: i

        integer  :: dualid, nall_id, nallpoints, eigf_id
        integer  :: global_id, rho_id, u_id, v_id, w_id, e_id, turb_id, turb2_id
        integer  :: rhoi_id, ui_id, vi_id, wi_id, ei_id, turbi_id, turb2i_id
        integer  :: error_rho_id, error_u_id, error_v_id, error_w_id, error_e_id
        integer  :: error_turb_id, error_turb2_id
        integer  :: nownpoints, nown_id
        ! Eigenvalues are stored in dual grid order.
        ! Data has to be reordered to prim grid order

        ! Get dual grid local ids for sorting
        call check( nf90_open(dualgrid, NF90_NOWRITE, dualid) )
        call check( nf90_inq_dimid(dualid, 'nallpoints', nall_id) )
        call check( nf90_inq_dimid(dualid, 'nownpoints', nown_id) )
        call check( nf90_inquire_dimension(dualid, nall_id, len=nallpoints) )
        call check( nf90_inquire_dimension(dualid, nown_id, len=nownpoints) )
        call check( nf90_close(dualid) )

        ! write pval file

        ! open file
        call check( nf90_create(trim(outfile), nf90_clobber, eigf_id) )

        ! define dimensions and variables
        if (ndim == 2) then
            allocate(var(nownpoints*2))
            call check( nf90_def_dim(eigf_id, 'no_of_points', nownpoints*2, nown_id ) )
        else
            allocate(var(nownpoints))
            call check( nf90_def_dim(eigf_id, 'no_of_points', nownpoints, nown_id ) )
        endif

        call check( nf90_def_var(eigf_id, 'global_id', NF90_INT, nown_id, global_id) )
        call check( nf90_put_att(eigf_id, global_id, 'scale_to_non_dimensional', 1._RP) )
        call check( nf90_put_att(eigf_id, global_id, 'scale_to_dimensional_SI', 1._RP) )

        !! Create netCDF variables
        call check( nf90_def_var(eigf_id, 'rho', NF90_DOUBLE, nown_id, rho_id) )
        call check( nf90_put_att(eigf_id, rho_id, 'scale_to_non_dimensional', 1._RP) )
        call check( nf90_put_att(eigf_id, rho_id, 'scale_to_dimensional_SI', 1._RP) )

        call check( nf90_def_var(eigf_id, 'u', NF90_DOUBLE, nown_id, u_id) )
        call check( nf90_put_att(eigf_id, u_id, 'scale_to_non_dimensional', 1._RP) )
        call check( nf90_put_att(eigf_id, u_id, 'scale_to_dimensional_SI', 1._RP) )

        call check( nf90_def_var(eigf_id, 'w', NF90_DOUBLE, nown_id, w_id) )
        call check( nf90_put_att(eigf_id, w_id, 'scale_to_non_dimensional', 1._RP) )
        call check( nf90_put_att(eigf_id, w_id, 'scale_to_dimensional_SI', 1._RP) )

        call check( nf90_def_var(eigf_id, 'e', NF90_DOUBLE, nown_id, e_id) )
        call check( nf90_put_att(eigf_id, e_id, 'scale_to_non_dimensional', 1._RP) )
        call check( nf90_put_att(eigf_id, e_id, 'scale_to_dimensional_SI', 1._RP) )

        call check( nf90_def_var(eigf_id, 'rho_i', NF90_DOUBLE, nown_id, rhoi_id) )
        call check( nf90_put_att(eigf_id, rhoi_id, 'scale_to_non_dimensional', 1._RP) )
        call check( nf90_put_att(eigf_id, rhoi_id, 'scale_to_dimensional_SI', 1._RP) )

        call check( nf90_def_var(eigf_id, 'u_i', NF90_DOUBLE, nown_id, ui_id) )
        call check( nf90_put_att(eigf_id, ui_id, 'scale_to_non_dimensional', 1._RP) )
        call check( nf90_put_att(eigf_id, ui_id, 'scale_to_dimensional_SI', 1._RP) )

        call check( nf90_def_var(eigf_id, 'w_i', NF90_DOUBLE, nown_id, wi_id) )
        call check( nf90_put_att(eigf_id, wi_id, 'scale_to_non_dimensional', 1._RP) )
        call check( nf90_put_att(eigf_id, wi_id, 'scale_to_dimensional_SI', 1._RP) )

        call check( nf90_def_var(eigf_id, 'e_i', NF90_DOUBLE, nown_id, ei_id) )
        call check( nf90_put_att(eigf_id, ei_id, 'scale_to_non_dimensional', 1._RP) )
        call check( nf90_put_att(eigf_id, ei_id, 'scale_to_dimensional_SI', 1._RP) )

        call check( nf90_def_var(eigf_id, 'error_rho', NF90_DOUBLE, nown_id, error_rho_id) )
        call check( nf90_put_att(eigf_id, error_rho_id, 'scale_to_non_dimensional', 1._RP) )
        call check( nf90_put_att(eigf_id, error_rho_id, 'scale_to_dimensional_SI', 1._RP) )

        call check( nf90_def_var(eigf_id, 'error_u', NF90_DOUBLE, nown_id, error_u_id) )
        call check( nf90_put_att(eigf_id, error_u_id, 'scale_to_non_dimensional', 1._RP) )
        call check( nf90_put_att(eigf_id, error_u_id, 'scale_to_dimensional_SI', 1._RP) )

        call check( nf90_def_var(eigf_id, 'error_w', NF90_DOUBLE, nown_id, error_w_id) )
        call check( nf90_put_att(eigf_id, error_w_id, 'scale_to_non_dimensional', 1._RP) )
        call check( nf90_put_att(eigf_id, error_w_id, 'scale_to_dimensional_SI', 1._RP) )

        call check( nf90_def_var(eigf_id, 'error_e', NF90_DOUBLE, nown_id, error_e_id) )
        call check( nf90_put_att(eigf_id, error_e_id, 'scale_to_non_dimensional', 1._RP) )
        call check( nf90_put_att(eigf_id, error_e_id, 'scale_to_dimensional_SI', 1._RP) )

        if (neq >= 5) then
            call check( nf90_def_var(eigf_id, 'turb1', NF90_DOUBLE, nown_id, turb_id) )
            call check( nf90_put_att(eigf_id, turb_id, 'scale_to_non_dimensional', 1._RP) )
            call check( nf90_put_att(eigf_id, turb_id, 'scale_to_dimensional_SI', 1._RP) )

            call check( nf90_def_var(eigf_id, 'turb1_i', NF90_DOUBLE, nown_id, turbi_id) )
            call check( nf90_put_att(eigf_id, turbi_id, 'scale_to_non_dimensional', 1._RP) )
            call check( nf90_put_att(eigf_id, turbi_id, 'scale_to_dimensional_SI', 1._RP) )

            call check( nf90_def_var(eigf_id, 'error_turb1', NF90_DOUBLE, nown_id, error_turb_id) )
            call check( nf90_put_att(eigf_id, error_turb_id, 'scale_to_non_dimensional', 1._RP) )
            call check( nf90_put_att(eigf_id, error_turb_id, 'scale_to_dimensional_SI', 1._RP) )
        endif
        if (neq == 6) then
            call check( nf90_def_var(eigf_id, 'turb2', NF90_DOUBLE, nown_id, turb2_id) )
            call check( nf90_put_att(eigf_id, turb2_id, 'scale_to_non_dimensional', 1._RP) )
            call check( nf90_put_att(eigf_id, turb2_id, 'scale_to_dimensional_SI', 1._RP) )

            call check( nf90_def_var(eigf_id, 'turb2_i', NF90_DOUBLE, nown_id, turb2i_id) )
            call check( nf90_put_att(eigf_id, turb2i_id, 'scale_to_non_dimensional', 1._RP) )
            call check( nf90_put_att(eigf_id, turb2i_id, 'scale_to_dimensional_SI', 1._RP) )

            call check( nf90_def_var(eigf_id, 'error_turb2', NF90_DOUBLE, nown_id, error_turb2_id) )
            call check( nf90_put_att(eigf_id, error_turb2_id, 'scale_to_non_dimensional', 1._RP) )
            call check( nf90_put_att(eigf_id, error_turb2_id, 'scale_to_dimensional_SI', 1._RP) )
        endif

        if (ndim == 3) then
            call check( nf90_def_var(eigf_id, 'v', NF90_DOUBLE, nown_id, v_id) )
            call check( nf90_put_att(eigf_id, v_id, 'scale_to_non_dimensional', 1._RP) )
            call check( nf90_put_att(eigf_id, v_id, 'scale_to_dimensional_SI', 1._RP) )

            call check( nf90_def_var(eigf_id, 'v_i', NF90_DOUBLE, nown_id, vi_id) )
            call check( nf90_put_att(eigf_id, vi_id, 'scale_to_non_dimensional', 1._RP) )
            call check( nf90_put_att(eigf_id, vi_id, 'scale_to_dimensional_SI', 1._RP) )

            call check( nf90_def_var(eigf_id, 'error_v', NF90_DOUBLE, nown_id, error_v_id) )
            call check( nf90_put_att(eigf_id, error_v_id, 'scale_to_non_dimensional', 1._RP) )
            call check( nf90_put_att(eigf_id, error_v_id, 'scale_to_dimensional_SI', 1._RP) )
        end if

        ! End of definitions (don't forget this line!)
        call check( nf90_enddef(eigf_id) )

        ! WRITE VARIABLES
        if (ndim == 2) then
            ! sort and duplicate arrays in the 2D case
            ! rho
            var(1:nownpoints) = 0.0
            var(localid+1) = real(eigvec(:,1))
            ! var(1:nownpoints) = real(eigvec(localid(1:nownpoints)+1,1))
            var(nownpoints+1:2*nownpoints) = var(1:nownpoints)
            call check( nf90_put_var(eigf_id, rho_id, var) )

            ! rhoU
            var(1:nownpoints) = 0.0
            var(localid+1) = real(eigvec(:,2))
            var(nownpoints+1:2*nownpoints) = var(1:nownpoints)
            call check( nf90_put_var(eigf_id, u_id, var) )

            ! rhoW
            var(1:nownpoints) = 0.0
            var(localid+1) = real(eigvec(:,3))
            var(nownpoints+1:2*nownpoints) = var(1:nownpoints)
            call check( nf90_put_var(eigf_id, w_id, var) )

            ! rhoE
            var(1:nownpoints) = 0.0
            var(localid+1) = real(eigvec(:,4))
            var(nownpoints+1:2*nownpoints) = var(1:nownpoints)
            call check( nf90_put_var(eigf_id, e_id, var) )

            ! rho_i
            var(1:nownpoints) = 0.0
            var(localid+1) = aimag(eigvec(:,1))
            var(nownpoints+1:2*nownpoints) = var(1:nownpoints)
            call check( nf90_put_var(eigf_id, rhoi_id, var) )

            ! rhoU_i
            var(1:nownpoints) = 0.0
            var(localid+1) = aimag(eigvec(:,2))
            var(nownpoints+1:2*nownpoints) = var(1:nownpoints)
            call check( nf90_put_var(eigf_id, ui_id, var) )

            ! rhoW_i
            var(1:nownpoints) = 0.0
            var(localid+1) = aimag(eigvec(:,3))
            var(nownpoints+1:2*nownpoints) = var(1:nownpoints)
            call check( nf90_put_var(eigf_id, wi_id, var) )

            ! rhoE_i
            var(1:nownpoints) = 0.0
            var(localid+1) = aimag(eigvec(:,4))
            var(nownpoints+1:2*nownpoints) = var(1:nownpoints)
            call check( nf90_put_var(eigf_id, ei_id, var) )

            ! Error_rho
            var(1:nownpoints) = 0.0
            var(localid+1) = real(error(:,1))
            var(nownpoints+1:2*nownpoints) = var(1:nownpoints)
            call check( nf90_put_var(eigf_id, error_rho_id, var) )

            ! Error_rhoU
            var(1:nownpoints) = 0.0
            var(localid+1) = real(error(:,2))
            var(nownpoints+1:2*nownpoints) = var(1:nownpoints)
            call check( nf90_put_var(eigf_id, error_u_id, var) )

            ! Error_rhoW
            var(1:nownpoints) = 0.0
            var(localid+1) = real(error(:,3))
            var(nownpoints+1:2*nownpoints) = var(1:nownpoints)
            call check( nf90_put_var(eigf_id, error_w_id, var) )

            ! Error_rhoE
            var(1:nownpoints) = 0.0
            var(localid+1) = real(error(:,4))
            var(nownpoints+1:2*nownpoints) = var(1:nownpoints)
            call check( nf90_put_var(eigf_id, error_e_id, var) )

            if (neq >= 5) then
                ! rhoT1
                var(1:nownpoints) = 0.0
                var(localid+1) = real(eigvec(:,5))
                var(nownpoints+1:2*nownpoints) = var(1:nownpoints)
                call check( nf90_put_var(eigf_id, turb_id, var) )
                ! rhoT1_i
                var(1:nownpoints) = 0.0
                var(localid+1) = aimag(eigvec(:,5))
                var(nownpoints+1:2*nownpoints) = var(1:nownpoints)
                call check( nf90_put_var(eigf_id, turbi_id, var) )
                ! Error_rhoT1
                var(1:nownpoints) = 0.0
                var(localid+1) = real(error(:,5))
                var(nownpoints+1:2*nownpoints) = var(1:nownpoints)
                call check( nf90_put_var(eigf_id, error_turb_id, var) )
            endif

            if (neq == 6) then
                ! rhoT2
                var(1:nownpoints) = 0.0
                var(localid+1) = real(eigvec(:,6))
                var(nownpoints+1:2*nownpoints) = var(1:nownpoints)
                call check( nf90_put_var(eigf_id, turb2_id, var) )
                ! rhoT2_i
                var(1:nownpoints) = 0.0
                var(localid+1) = aimag(eigvec(:,6))
                var(nownpoints+1:2*nownpoints) = var(1:nownpoints)
                call check( nf90_put_var(eigf_id, turb2i_id, var) )
                ! Error_rhoT2
                var(1:nownpoints) = 0.0
                var(localid+1) = real(error(:,6))
                var(nownpoints+1:2*nownpoints) = var(1:nownpoints)
                call check( nf90_put_var(eigf_id, error_turb2_id, var) )
            endif
            ! Global_ID
            call check( nf90_put_var(eigf_id, global_id, [(i, i=0,2*nownpoints-1)]))

        else
            ! sort arrays in the 3D case
            ! var(1:nownpoints) = real(eigvec(localid(1:nownpoints)+1,1))
            ! call check( nf90_put_var(eigf_id, rho_id, var) )

            ! rho
            var(1:nownpoints) = 0.0
            var(localid+1) = real(eigvec(:,1))
            call check( nf90_put_var(eigf_id, rho_id, var) )

            ! rhoU
            var(1:nownpoints) = 0.0
            var(localid+1) = real(eigvec(:,2))
            call check( nf90_put_var(eigf_id, u_id, var) )

            ! rhoV
            var(1:nownpoints) = 0.0
            var(localid+1) = real(eigvec(:,3))
            call check( nf90_put_var(eigf_id, v_id, var) )

            ! rhoW
            var(1:nownpoints) = 0.0
            var(localid+1) = real(eigvec(:,4))
            call check( nf90_put_var(eigf_id, w_id, var) )

            ! rhoE
            var(1:nownpoints) = 0.0
            var(localid+1) = real(eigvec(:,5))
            call check( nf90_put_var(eigf_id, e_id, var) )

            ! rho_i
            var(1:nownpoints) = 0.0
            var(localid+1) = aimag(eigvec(:,1))
            call check( nf90_put_var(eigf_id, rhoi_id, var) )

            ! rhoU_i
            var(1:nownpoints) = 0.0
            var(localid+1) = aimag(eigvec(:,2))
            call check( nf90_put_var(eigf_id, ui_id, var) )

            ! rhoV_i
            var(1:nownpoints) = 0.0
            var(localid+1) = aimag(eigvec(:,3))
            call check( nf90_put_var(eigf_id, vi_id, var) )

            ! rhoW_i
            var(1:nownpoints) = 0.0
            var(localid+1) = aimag(eigvec(:,4))
            call check( nf90_put_var(eigf_id, wi_id, var) )

            ! rhoE_i
            var(1:nownpoints) = 0.0
            var(localid+1) = aimag(eigvec(:,5))
            call check( nf90_put_var(eigf_id, ei_id, var) )

            ! Error_rho
            var(1:nownpoints) = 0.0
            var(localid+1) = real(error(:,1))
            call check( nf90_put_var(eigf_id, error_rho_id, var) )

            ! Error_rhoU
            var(1:nownpoints) = 0.0
            var(localid+1) = real(error(:,2))
            call check( nf90_put_var(eigf_id, error_u_id, var) )

            ! Error_rhoV
            var(1:nownpoints) = 0.0
            var(localid+1) = real(error(:,3))
            call check( nf90_put_var(eigf_id, error_v_id, var) )

            ! Error_rhoW
            var(1:nownpoints) = 0.0
            var(localid+1) = real(error(:,4))
            call check( nf90_put_var(eigf_id, error_w_id, var) )

            ! rhoE
            var(1:nownpoints) = 0.0
            var(localid+1) = real(eigvec(:,5))
            call check( nf90_put_var(eigf_id, error_e_id, var) )

            if (neq >= 6) then
                ! rhoT1
                var(1:nownpoints) = 0.0
                var(localid+1) = real(eigvec(:,6))
                call check( nf90_put_var(eigf_id, turb_id, var) )
                ! rhoT1_i
                var(1:nownpoints) = 0.0
                var(localid+1) = aimag(eigvec(:,6))
                call check( nf90_put_var(eigf_id, turbi_id, var) )
                ! Error_rhoT1
                var(1:nownpoints) = 0.0
                var(localid+1) = real(error(:,6))
                call check( nf90_put_var(eigf_id, error_turb_id, var) )
            endif

            if (neq == 7) then
                ! rhoT2
                var(1:nownpoints) = 0.0
                var(localid+1) = real(eigvec(:,7))
                call check( nf90_put_var(eigf_id, turb2_id, var) )
                ! rhoT2_i
                var(1:nownpoints) = 0.0
                var(localid+1) = aimag(eigvec(:,7))
                call check( nf90_put_var(eigf_id, turb2i_id, var) )
                ! Error_rhoT2
                var(1:nownpoints) = 0.0
                var(localid+1) = real(error(:,7))
                call check( nf90_put_var(eigf_id, error_turb2_id, var) )
            endif
            ! Global_ID
            call check( nf90_put_var(eigf_id, global_id, [(i, i=0,nownpoints-1)]))
        endif
        !

        call check( nf90_close(eigf_id) )

        print*, "   eigenvector saved in: ", trim(outfile)

    end subroutine
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
    subroutine check(status)
        ! NetCDF check-error function
        integer, intent ( in) :: status

        if(status /= nf90_noerr) then
            print *, trim(nf90_strerror(status))
            stop "Stopped"
        end if
    end subroutine check
!-------------------------------------------------------------------------------
end module
